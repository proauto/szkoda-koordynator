(function ($) {
    function parseModalContent($modal, data) {
        var tpl = _.template($modal.find('.content-template').html());
        var parsed = tpl(data);
        $modal.find('.content-inject').empty().append(parsed);
    }

    function caseRecalculateClickEventHandler(e) {
        e.stopPropagation();
        var $modal = $(e.currentTarget).parents('.modal');
        var form = $modal.find('form');
        $modal.find(".loading-overlay").addClass('loading-overlay--visible');


        $form = form;
        var cbfn = function (data) {
            if ("success" === data.data.status) {
                var calculation = data.data.data['Module-Data'].calculation;
                var form = $form;
                var $modal = $(form).parents('.modal');
                $modal.find(".loading-overlay").removeClass('loading-overlay--visible');
                form.find('[name="appraiser-proposed-value"]').val(calculation.value).attr('data-backup-value', calculation.value);
                form.find('[name="client-proposed-value"]').val(calculation.client);
                form.find('[name="margin-value"]').val(calculation.margin);
                form.find('[name="caid"]').val(calculation.id);
                form.find('[name="caid"]').val(calculation.id);

                form.find('[name="calculation-created"]').val(calculation.created.date);

                var editButton = $form.parents('.modal').find('#case-edit-execute');
                var calculateButton = $form.parents('.modal').find('.btn-recalculate');
                editButton.attr("disabled", false);
                calculateButton.attr("disabled", true);
            }
        }
        var request = new paApiDataRequest('editCase', {
            endpoint: "v1/data/calculation/create",
            headers: {
                value: form.find('[name="appraiser-proposed-value"]').val(),
                cid: form.find('[name="cid"]').val(),
                cuid: form.find('[name="cuid"]').val(),
            },
            type: "post",
            callback: cbfn
        }, false);

        request.send();
    }

    function editModalForm_value_changeEvent(e) {
        var recalculateButton = $(e.currentTarget).parents('.modal').find('.btn-recalculate');
        var editButton = $(e.currentTarget).parents('.modal').find('#case-edit-execute');
        var button = $(e.currentTarget).parents('.modal').find('.btn-primary');
        var value = $(e.currentTarget).parents('.modal').find('[name="appraiser-proposed-value"]').val();
        var valueBackup = $(e.currentTarget).parents('.modal').find('[name="appraiser-proposed-value"]').attr('data-backup-value');
        var status = value !== valueBackup ? 1 : 0;
        button.attr('data-recalculate', status).attr('disabled', status);

        if (0 === status) {
            recalculateButton.attr("disabled", true);
        } else {
            recalculateButton.removeAttr("disabled");
            editButton.attr("disabled", true);
        }

        if (value == valueBackup) {
            editButton.attr("disabled", false);
        }
    }

    function parseCaseEditModalContent($modal, data) {
        var nameMap = {
            "sessionID": "cid",
            "owner": "case-owner",
            "calculation": "appraiser-proposed-value",
            "status": "case-status"
        }
        var form = $modal.find("form");
        var $caseOwner = form.find("[name='case-owner']");
        $caseOwner.children('[value="' + data['owner'].ID + '"]').attr('selected', 'selected');

        form.find("[name='cuid']").val(geConfig.apiAccess.uid);
        for (var i in data) {
            var name = undefined === nameMap[i] ? i : nameMap[i];
            var formEl = form.find("[name=" + name + "]");

            if ("appraiser-proposed-value" === name) {
                formEl.unbind("change");
                formEl.unbind("keydown");
                formEl.on("change", editModalForm_value_changeEvent);
                formEl.val(data.calculation.value);
                form.find("[name='client-proposed-value']").val(data.calculation.client);
                form.find("[name='margin-value']").val(data.calculation.margin);
                form.find("[name='caid']").val(data.calculation.id);
                continue;
            }

            if ("case-owner" === name) {
                formEl.find("[value=\"" + data.owner.ID + "\"]").attr("selected", "selected");
                continue;
            }

            if (formEl.is("*")) {
                formEl.val(data[i]);
            }
        }

        var $valueInput = $modal.find('[name="appraiser-proposed-value"]');
        $valueInput.attr('data-backup-value', data.calculation.value);

        var $loadingOverlay = $modal.find('.loading-overlay');
        $loadingOverlay.removeClass("loading-overlay--visible").on("click", function (e) {
            e.stopPropagation();
        });

        if ("" === data.calculation.id) {
            form.parents('.modal').find('#case-edit-execute').attr("disabled", true);
        }

    }


    window.case_status_valuesFilter = function (value, form) {
        if (value) {
            return form.find('[name="case-status"]').children('[value="' + value + '"]').html();
        }
        return value;
    }

    function valuesFilter(value, filterName, form) {
        //normalize
        filterName = filterName.replace(['-'], ["_"]) + '_valuesFilter';
        return "function" === typeof window[filterName] ? window[filterName](value, form) : value;
    }

    function caseEditExecuteClickEventHandler(e) {
        var $modal = $(e.currentTarget).parents('.modal');
        $modal.find(".loading-overlay").addClass('loading-overlay--visible');
        var values = $($modal.find('form')).serializeArray();

        valuesObj = {}
        for (var i in values) valuesObj[values[i].name] = values[i].value;
        modal = $modal;
        var cbfn = function (data) {
            modal = $modal;
            $modal.find(".loading-overlay").removeClass('loading-overlay--visible');
            var values = $($modal.find('form')).serializeArray();
            valuesObj = {}
            for (var i in values) valuesObj[values[i].name] = values[i].value;
            var trid = $modal.attr('data-trid');
            var rowValuesObj = jQuery("#example").DataTable().row(jQuery('tr#' + trid)[0]).data()
            var modifiedDate = data.data.data["Module-Data"].modified.date

            if (trid) {
                var order = {
                    "id": trid,
                    "status": valuesFilter(valuesObj['case-status'], 'case-status', $modal.find('form')),
                    "owner": {
                        displayName: "dupa"
                    },
                    "created": rowValuesObj.created,
                    "modified": {
                        date: modifiedDate
                    },

                    "collision-in-pl": undefined === valuesObj['collision-in-pl'] ? '' : valuesObj['collision-in-pl'],
                    "insurance-type": undefined === valuesObj['insurance-type'] ? '' : valuesObj['insurance-type'],

                    "client-email": valuesObj['client-email'],
                    "client-full-name": valuesObj['client-full-name'],
                    "client-phone-number": valuesObj['client-phone-number'],

                    "calculation": {
                        'created': {
                            date: "" === valuesObj['calculation-created'] ? rowValuesObj.calculation.created.date : valuesObj['calculation-created']
                        },
                        'value': valuesObj["appraiser-proposed-value"],
                        "margin": valuesObj["margin-value"],
                        'client': valuesObj["client-proposed-value"]
                    }
                    /*
                    "manager-case-description": 0,
                    */
                }


                $("#example").dataTable().fnUpdate(order, $("tr#" + trid).index());
                markRow(trid, 'updated');
            }
            modal.find('[data-dismiss="modal"]').trigger('click');
        }

        var request = new paApiDataRequest('theList', {
            endpoint: "v1/data/calculation/update",
            type: "post",
            callback: cbfn,
            headers: {encodedData: btoa(encodeURI(JSON.stringify(valuesObj)))}
        }, false);

        request.send();
    }

    function caseRemoveExecuteClickEventHandler(e) {
        var $modal = $(e.currentTarget).parents('.modal');
        $modal.find(".loading-overlay").addClass('loading-overlay--visible');
        var trid = $modal.attr('data-trid');
        modal = $modal;
        var cbfn = function (data) {
            modal = $modal;
            $modal.find(".loading-overlay").removeClass('loading-overlay--visible');
            var trid = $modal.attr('data-trid');
            if (trid) {
                jQuery('#'+trid).remove();
            }
            modal.find('[data-dismiss="modal"]').trigger('click');
        }

        var request = new paApiDataRequest('remove' + trid, {
            endpoint: "v1/data/calculation/remove",
            type: "post",
            callback: cbfn,
            headers: {cid: trid}
        }, false);

        request.send();
    }

    window.columnDateRenderer = function (data, type, dataToSet) {
        if ("" === data) return '';
        var date = new Date(Date.parse(data));
        var month = date.getMonth(); //Be careful! January is 0 not 1
        var year = date.getFullYear();

        function pad(n) {
            return n < 10 ? '0' + n : n;
        }

        var dateString = pad(date.getDate()) + "-" + pad(month + 1) + "-" + year;
        var timeString = pad(date.getHours()) + ':' + pad(date.getMinutes()) + ':' + pad(date.getSeconds());

        return dateString + " " + timeString;
    }

    $(document).ready(function () {
        $("#case-remove-execute").on("click", caseRemoveExecuteClickEventHandler);
        $("#case-edit-execute").on("click", caseEditExecuteClickEventHandler);
        $("#case-recalculate").on("click", caseRecalculateClickEventHandler);
        window.dt = $('#example').DataTable({
            language: {
                "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Polish.json"
            },
            rowId: 'id',
            ajax: function (data, callback, settings) {
                $callback = callback;
                var cbfn = function (data) {
                    $callback(data.data);
                }
                var request = new paApiDataRequest('theList', {
                    endpoint: "v1/data/list/get",
                    type: "post",
                    callback: cbfn
                }, false);

                request.send(callback);

            },

            aoColumns: [
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                {"orderSequence": ["desc", "asc"]},
                null
            ],

            columns: [
                {data: 'id'},
                //case status
                {
                    "mData": null,
                    "bSortable": true,
                    "mRender": function (o) {
                        return o.status;
                    }
                },
                {data: 'owner.displayName'},
                {data: 'created.date', render: window.columnDateRenderer},
                {data: 'modified.date', render: window.columnDateRenderer},
                {data: 'client-full-name'},
                {data: 'client-phone-number'},
                {data: 'client-email'},
                {data: 'insurance-type'},
                {data: 'collision-in-pl'},

                {data: 'calculation.created.date', render: window.columnDateRenderer},
                {data: 'calculation.value'},
                {data: 'calculation.margin'},
                {data: 'calculation.client'},
                {
                    "mData": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        var a = geConfig.caps.administrator;
                        if (a) {
                            a = '<a class="case-action-action case-remove-data" title="Usuń" href="#" data-id="' + o.id + '"><i class="fas fa-trash"></i></a>'
                        }
                        return '<div>'
                            + '<a class="case-action-action case-download-files" title="Pobierz pliki" href="#" data-id="' + o.id + '"><i class="fas fa-download"></i></a>'
                            + '<a class="case-action-action case-edit-data" title="Edytuj" href="#" data-id="' + o.id + '"><i class="fas fa-edit"></i></a>'
                            + '<a class="case-action-action case-send-link-to-appraiser" title="Wyślij link do rzeczoznawcy" href="#" data-id="' + o.id + '"><i class="fas fa-envelope"></i></a>'
                            + a
                        '</div>';
                    }
                }

            ]

        });
    });

    function parseModalContent($modal, data) {
        var tpl = _.template($modal.find('.content-template').html());
        var parsed = tpl(data);
        $modal.find('.content-inject').empty().append(parsed);
    }

    $('#example').on('draw.dt', function () {

        var appraiserResendLinkHandler = function (e) {
            e.stopPropagation();
            var iCaseId = $(e.currentTarget).data("id");
            $modal = $("#modal-appraiser-send-link");
            var cbfn = function (data) {
                parseModalContent($modal, {
                    appraiserUrl: data.data.data["Module-Data"].appraiserUrl
                });
                $modal.modal("show");
            }
            var request = new paApiDataRequest('theCase' + iCaseId, {
                endpoint: "v1/data/szkoda/appraiser-send-link",
                headers: {
                    "Caseid": iCaseId
                },
                type: "post",
                callback: cbfn
            }, false);

            request.send();
        }

        var downloadFilesHandler = function (e) {
            e.stopPropagation();
            var iCaseId = $(e.currentTarget).data("id");
            $modal = $("#modal-case-files");

            var cbfn = function (data) {
                try {
                    var files = data.data.data["Module-Data"].files;
                } catch (er) {
                }
                if (files) {
                    parseModalContent($modal, {files: files});
                    $modal.modal("show");
                } else {
                    debugger
                }
            }
            var request = new paApiDataRequest('theCase' + iCaseId, {
                endpoint: "v1/data/szkoda/files-modal",
                headers: {
                    "Caseid": iCaseId
                },
                type: "post",
                callback: cbfn
            }, false);

            request.send();

        }
        $('.case-download-files').unbind('click');
        $('.case-download-files').on('click', downloadFilesHandler);

        var removeDatahandler = function (e) {
            e.stopPropagation();
            var trid = parseInt($(e.currentTarget).parents("tr[id]").attr("id"));
            markRow(trid, 'selected');
            var iCaseId = $(e.currentTarget).data("id");
            var table = jQuery('#example').DataTable();
            var data = table.row(jQuery(this).parents('tr')[0]).data();
            var modal = $("#modal-remove-case");

            modal.attr('data-trid', trid);
            parseModalContent(modal, data);
            modal.modal("show")
        }

        var editDatahandler = function (e) {
            e.stopPropagation();
            var trid = parseInt($(e.currentTarget).parents("tr[id]").attr("id"));
            markRow(trid, 'selected');
            var iCaseId = $(e.currentTarget).data("id");
            var table = jQuery('#example').DataTable();
            var data = table.row(jQuery(this).parents('tr')[0]).data();
            var modal = $("#modal-edit-case");

            modal.attr('data-trid', trid);
            parseModalContent(modal, data);
            parseCaseEditModalContent(modal, data);
            modal.modal("show")
        }
        $('.case-edit-data').unbind('click');
        $('.case-edit-data').on('click', editDatahandler);

        $('.case-remove-data').unbind('click');
        $('.case-remove-data').on('click', removeDatahandler);

        $('.case-send-link-to-appraiser').unbind('click');
        $('.case-send-link-to-appraiser').on('click', appraiserResendLinkHandler);
        $('[data-dismiss="modal"]').on('click', function(){
            $("#example").find('tr[id]').removeClass('selected');
        });
    });

    function markRow(trid, mode) {
        var $tr = $("#" + trid);
        $("#example").find('tr[id]').not($tr).removeClass(mode);
        if ($tr.is('.' + mode)) $tr.removeClass(mode);
        else $tr.addClass(mode);
    }

})(jQuery)