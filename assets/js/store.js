Store = new Vuex.Store({
    state: {
        VueComponentsLoaded: window['VueComponentsLoaded'],
        VueViewsLoaded: window['VueViewsLoaded'],
        apiAccess: window['apiAccess'],
        apiToken: false,
        apiTokenEndpoint: 'v1/auth/access-token'
    },

    mutations: {
        apiTokenRefresh: function (state, val) {
            if (undefined !== val) state.apiToken = val;
        },

        VueViewLoadedAdd: function (state, val) {
            if (state.VueViewsLoaded.indexOf(val) === -1) state.VueViewsLoaded.push(val);
        },

        VueComponentsLoadedAdd: function (state, val) {
            if (state.VueComponentsLoaded.indexOf(val) === -1) state.VueComponentsLoaded.push(val);
        }
    },

    getters: {
        apiTokenEndpoint: function (data) {
            return data.apiTokenEndpoint;
        },
        apiAccess: function (data) {
            return data.apiAccess;
        },

        apiToken: function (data) {
            return data.apiToken;
        },

        VueViewLoadedG: function (data) {
            return data.VueViewsLoaded;
        },

        VueComponentsLoadedG: function (data) {
            return data.VueComponentsLoaded;
        }
    },

    actions: {}

});