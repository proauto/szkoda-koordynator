(function () {
    var LocStorMng = function (collectionName, type) {
        this.active = true;
        this.type = type || 'sessionStorage';
        this.currentItem = null
        this.collectionName = collectionName;
        this.stats = {}

        this.init = function () {
            if (undefined === window[this.type]) this.active = false;

        }

        /**
         * Local storage cache item refresh in background.
         * apiDataRequest object in silent mode did not communicate with the app just inserts fresh data into local storage cache
         * @param originName
         */
        this.refresh = function (originName) {

            if (undefined === originName) return;
            var requestData = this.getRequestData(originName);
            if (requestData) {
                var refreshRequest = new apiDataRequest(requestData.headers['Ge-Data-Origin'], requestData.data, this.$root, true);
                $apiDataRequest = refreshRequest;
                refreshRequest.makeDataRequest();
            }
        }

        this.getRequestUrl = function (endpoint) {
            return window.location.protocol + '//' + window.location.host + '/' + geConfig.rest.routePrefix + endpoint;
        }

        this.add = function (originName, storageItem, requestObject) {
            var alive = parseInt(storageItem.response.data.data["Cache-Alive"]);
            if (0 === alive) return;

            var jsonString = storageItem.getJSONString();

            //requestObject is for preloading service, keeps essential data for creating same request again
            if (undefined !== requestObject) {
                var requestData = requestObject.getEssentialData();
                var obj = JSON.parse(jsonString);
                obj = Object.assign(obj, {requestData: requestData})
                jsonString = JSON.stringify(obj);
            }
            //cache stats
            var cacheStats = new window.LocStorStats(this.collectionName, this.type);
            if (cacheStats.addCheck(jsonString)) {
                window[this.type].setItem(this.collectionName + '_' + originName, jsonString);
                cacheStats.refreshStats();
            } else {
                cacheStats.clearCache();
                if (cacheStats.addCheck(jsonString)) {
                    window[this.type].setItem(this.collectionName + '_' + originName, jsonString);
                    cacheStats.refreshStats();
                }
            }
        }

        this.get = function (item) {
            if (!this.active) return null;
            var cached = window[this.type].getItem(this.collectionName + '_' + item);
            if (cached) {
                var storedItem = new window.LocStorItem(cached);
                return storedItem.isAlive() ? storedItem : null;
            }
            return null;
        }

        this.getRequestData = function (item) {
            if (!this.active) return null;
            var cached = window[this.type].getItem(this.collectionName + '_' + item);
            if (cached) {
                try {
                    var parsed = JSON.parse(cached)
                } catch (error) {
                }
                if (undefined !== parsed && undefined !== parsed.requestData) {
                    return parsed.requestData;
                }
            }
            return null;
        }

        this.remove = function (item) {
            debugger
        }

        this.genStats = function () {
            debugger
        }

        this.saveStats = function () {
            debugger
        }


        return this.init(collectionName);
    };

    window.LocStorMng = LocStorMng;
})()