(function ($) {
    $(document).ready(function () {

        if($(".loginFormWrapper form").is("*")) {

            window.dummyInterval = setInterval(function (){
                if($('.wpcf7-response-output').html() !== ""){
                    //tem solution to redirect after login. Adding listener to `wpcf7mailsent` event is not working in this case  for unknown reason
                    if($('.wpcf7-response-output').html() === "Logowanie powiodło się"){
                        clearInterval(window.dummyInterval);
                        setTimeout(function (){window.location.reload();}, 500)
                    }
                }

            }, 100);

        }
    });

})(jQuery)