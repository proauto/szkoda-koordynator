(function () {
    var LocStorItem = function (response, alive) {
        this.response = response;
        this.created = parseInt(Date.now() / 1000);
        this.output = {
            created: this.created,
            alive: this.created + (undefined === alive ? 3600 : alive),
            response: response
        }

        this.init = function (response) {
            if (typeof response === "string") {
                try {
                    var parsed = JSON.parse(response)
                } catch (error) {
                }
                if (undefined !== parsed) {
                    this.output = parsed;
                }
            }
        }

        this.getJSONString = function (response) {
            return JSON.stringify(this.output);
        }

        this.getResponse = function (response) {
            return this.output.response.data.data;
        }

        this.isAlive = function () {
            var now = parseInt(Date.now() / 1000);
            var until = this.output.alive;
            return now < until;
        }


        return this.init(response);
    };

    window.LocStorItem = LocStorItem;
})()