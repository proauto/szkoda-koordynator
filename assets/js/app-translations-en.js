(function () {

    var geTranslate = function (inputString) {

        this.name = 'geTranslate object';
        this.lang = 'en';

        this.tableIndex = [];
        this.table = {
            'key': 'value'
        };

        this.translate = function (inputStr) {
            return this.tableIndex.indexOf(inputStr) === -1 ? inputStr :
                (this.table[inputStr].length > 0 ? this.table[inputStr] : inputStr);
        };

        this.init = function () {
            for (var i in this.table) {
                this.tableIndex.push(i);
            }
            return this;
        };

        return this.init();
    };


    window.geTranslate = new geTranslate();

})();