var appRoutes = [
    {
        path: "/",
        name: "index",
        component: "index-view"
    },
    {
        path: "/not-found-404",
        name: "not-found-404",
        component: "not-found-404"
    },
    {
        path: "/restricted",
        name: "restricted",
        component: "restricted"
    }
]