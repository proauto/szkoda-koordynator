(function () {
    /*
        data format:
        {
            type: string
            headers: object({name: val ...})
        }

        silent mode - without Store integration
         */
    var paApiDataRequest = function (origin, data, silentMode) {
        this.errors = [];
        this.response = null;
        this.done = false;
        this.silentMode = silentMode || false;

        this.apiAccess = window.geConfig.apiAccess;
        this.apiToken = window.geConfig.apiToken;
        this.authEndpoint = 'v1/auth/access-token';

        this.origin = origin;
        this.data = data;
        this.type = undefined === data.type ? 'get' : data.type;
        this.headers = undefined === data.headers ? {} : data.headers;
        this.headers['Ge-Data-Origin'] = origin;
        if (undefined !== data['alive']) {
            this.headers['Cache-Alive'] = data['alive'];
        }

        this.send = function () {
            window.dataLoadedIndex.push(this.origin);
            typeof this.apiToken !== "string" ?
                this.aquireApiToken(this.makeDataRequest) : this.makeDataRequest();
        }

        this.addAuth = function () {
            if (this.apiToken) {
                this.headers['Authorization'] = "Bearer " + this.apiToken;
                return true;
            }
            return false;
        }

        this.makeDataRequest = function () {
            $this = this === window ? $this : this;
            if ($this.allowRequest()) {
                if ($this.apiToken) {
                    $this.headers['Authorization'] = "Bearer " + $this.apiToken;
                }
                $this.setLoadingMessage();
                axios[$this.type]($this.getRequestUrl(), {}, {headers: $this.headers})
                    .then($this.set)
                    .then($this.success)
                    .then($this.clean);
            }
        }

        this.setLoadingMessage = function () {
            if (undefined !== this.data.loadingMsg) {
                this.app.loadingMsg = this.data.loadingMsg;
            }
        }

        /*
        Not perfect technique to prevent doubled requests.
        * */
        this.allowRequest = function () {
            return true;
        }

        this.aquireApiToken = function (callbackFunction) {
            if (undefined !== this.apiAccess.uid && undefined !== this.apiAccess.domain) {
                var headers = {
                    uid: this.apiAccess.uid,
                    domain: this.apiAccess.domain,
                    u: 'self',
                    p: 'auth'
                };
            } else {
                var headers = {
                    u: this.apiAccess.user,
                    p: this.apiAccess.pass
                };
            }

            $this = this;
            axios.post(this.getAuthUrl(), {}, {headers: headers})
                .then(function (data) {
                    if (200 === data.status && "success" === data.data.status) {
                        $this.apiToken = data.data.data.access_token;
                        window.geConfig.apiToken = $this.apiToken;
                    }
                    return data;
                })
                .then(undefined !== callbackFunction ? callbackFunction : function () {
                });
        }

        this.clean = function (originName) {
        }

        this.actionPermissionDeniedError = function (data) {
            debugger
        }

        this.authOutdatedError = function (data) {
            this.aquireApiToken(this.makeDataRequest);
        }

        this.getEssentialData = function () {
            var headersWList = ['Ge-Data-Origin'];
            var headers = {};
            for (i in this.headers) {
                if (headersWList.indexOf(i) !== -1) {
                    headers[i] = this.headers[i];
                }
            }

            return {
                type: this.type,
                data: this.data,
                headers: headers
            }
        }

        this.success = function (data) {
            if (200 === data.status && "error" === data.data.status) {
                if (403 === data.data.code) {
                    $this.actionPermissionDeniedError(data);
                    return;
                }
                if (430 === data.data.code) {
                    $this.authOutdatedError(data);
                    return;
                }
            }
            if (200 === data.status && "success" === data.data.status) {
                var requestData = data.data.data;
                var originName = requestData['Ge-Data-Origin'];
                var moduleData = requestData['Module-Data'];
                var alive = requestData['Cache-Alive'];

                typeof $this.data.callback === "function" ? $this.data.callback(data) : false;
            }
            return originName;
        }

        this.set = function (data) {
            if (200 === data.status) {
                $this.response = data;
                $this.done = true;
            }
            return data;
        }

        this.getRequestUrl = function () {
            return window.location.protocol + '//' + window.location.host + '/' + geConfig.rest.routePrefix + this.data.endpoint;
        }
        this.getAuthUrl = function () {
            return window.location.protocol + '//' + window.location.host + '/' + geConfig.rest.routePrefix + this.authEndpoint;
        }

        this.getOrigin = function () {
            return this.origin;
        }

        this.getData = function () {
            return this.data;
        }
    }
    window.dataLoadedIndex = window.dataLoadedIndex || [];
    window.paApiDataRequest = paApiDataRequest;
})()