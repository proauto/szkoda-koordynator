(function ($, wpcf7) {

    function getFormValue(name, formData) {
        for (var i in formData) {
            var item = formData[i];
            if (name === item.name) {
                return item.value;
            }
        }
    }

    $(document).ready(function () {
        $("#calculate-case").on("click", calculateClickEventHandler);
        $("#case-accept").on("click", acceptCaseExecuteClickEventHandler);
        $("#case-deny").on("click", denyCaseExecuteClickEventHandler);

        $(".case-accept").on("click", calculationAcceptEventHandler);
        $(".case-deny").on("click", denyClickEventHandler);
        $(".edit-calculation").on("click", calculationEditEventHandler);
        $(".case-calc-show").on("click", calculationShowEventHandler);
        if (jQuery(".wpcf7").is("*"))
            jQuery(".wpcf7")[0].addEventListener('wpcf7mailsent', function (e) {debugger
                var formData = e.detail.inputs;
                var url = new URL(window.location.href);
                var sessionID = url.searchParams.get("cid");
                var value = getFormValue("appraiser-proposed-value", formData);
                var description = getFormValue("appraiser-case-description", formData);
                var hash = e.detail.apiResponse.posted_data_hash;
                $formData = formData;
                var restoreFormData = function () {
                    var formData = $formData;
                    for (var i in formData) {
                        var field = $('form').find("[name=\"" + formData[i]['name'] + "\"]");
                        if ("" === field.val())
                            field.val(formData[i]['value']);
                    }
                }

                setTimeout(restoreFormData, 250);

                var cbfn = function (data) {
                    $modal = $("#modal-case-calculation");
                    try {
                        var calculation = data.data.data["Module-Data"].calculation;
                    } catch (er) {
                    }
                    if (calculation) {
                        parseModalContent($modal, calculation);
                        window.currentCalculationID = calculation.id;
                        $modal.modal("show");
                    } else {
                        debugger
                    }


                }
                var request = new paApiDataRequest('theCaseCalculation' + sessionID, {
                    endpoint: "v1/data/calculation/get",
                    headers: {
                        "SessionID": sessionID,
                        "Value": value,
                        "Description": description,
                        "Hash": hash
                    },
                    type: "post",
                    callback: cbfn
                }, false);

                request.send();

            }, false);

    });

    function calculateClickEventHandler(e) {
        $("#case-calc-show").removeAttr('disabled');
        wpcf7.submit($('form')[0]);
    }

    function calculationAcceptEventHandler(e) {
        $("#modal-case-calculation").modal('hide');
        var $modal = $("#modal-case-calculation-accept");
        parseModalContent($modal, {});
        $modal.modal('show');
    }

    function calculationEditEventHandler(e) {
        jQuery(".modal:visible").modal('hide');
        $(".case-calc-show").removeAttr('disabled');
        wpcf7.clearResponse(jQuery('form')[0]);
    }

    function calculationShowEventHandler(e) {
        jQuery(".modal:visible").modal('hide');
        $("#modal-case-calculation").modal('show');
    }

    function caseClosedSuccessHandler() {
        window.location.reload();
    }

    function acceptCaseExecuteClickEventHandler(e) {
        var url = new URL(window.location.href);
        var sessionID = url.searchParams.get("cid");
        if (undefined === window.currentCalculationID) return;

        var cbfn = function (data) {
            if ("success" === data.data.status) {
                caseClosedSuccessHandler();
            }
        }
        var request = new paApiDataRequest('denyCase' + sessionID, {
            endpoint: "v1/data/calculation/accept",
            headers: {
                "SessionID": sessionID,
                "CalculationID": window.currentCalculationID,
            },
            type: "post",
            callback: cbfn
        }, false);

        request.send();
    }

    function denyCaseExecuteClickEventHandler(e) {
        var url = new URL(window.location.href);
        var sessionID = url.searchParams.get("cid");
        if (undefined === window.currentCalculationID) return;

        var cbfn = function (data) {
            if ("success" === data.data.status) {
                caseClosedSuccessHandler();
            }
        }
        var request = new paApiDataRequest('denyCase' + sessionID, {
            endpoint: "v1/data/calculation/deny",
            headers: {
                "SessionID": sessionID,
            },
            type: "post",
            callback: cbfn
        }, false);

        request.send();
    }

    function denyClickEventHandler(e) {
        $("#modal-case-calculation").modal('hide');
        var $modal = $("#modal-case-calculation-deny");
        debugger
        parseModalContent($modal, {});
        $modal.modal('show');
    }

    function parseModalContent($modal, data) {
        var tpl = _.template($modal.find('.content-template').html());
        var parsed = tpl(data);
        $modal.find('.content-inject').empty().append(parsed);
    }

})(jQuery, wpcf7)