<?php
/*
 Plugin Name: szkoda-koordynator
 Plugin URI:
 Description:
 Version: 0.1
 Author: Bartek GAAD Sosnowski
 Author URI:
 Text Domain: szkoda_koordynator
 */
define('__SZKODA_KOORDYNATOR_DIR__', __DIR__);
define('__SZKODA_KOORDYNATOR_PAGE__', 'manager');
define('__SZKODA_RZECZOZNAWCA_PAGE__', 'appraiser');


define('__SZKODA_KOORDYNATOR_WIDGET_OPTIONS_INIT__', '__SZKODA_KOORDYNATOR_WIDGET_OPTIONS_INIT__');
define('__SZKODA_KOORDYNATOR_WIDGET_SYSTEM_ELEMENTS_INIT__', '__SZKODA_KOORDYNATOR_WIDGET_SYSTEM_ELEMENTS_INIT__');
define('__SZKODA_KOORDYNATOR_URI__', explode("wp-content", get_stylesheet_directory_uri())[0] . "wp-content/plugins/" . basename(__SZKODA_KOORDYNATOR_DIR__));
file_exists(__SZKODA_KOORDYNATOR_DIR__ . "/../gendpoints2/vendor/autoload.php") ? require_once __SZKODA_KOORDYNATOR_DIR__ . "/../gendpoints2/vendor/autoload.php" : null;
file_exists(__DIR__ . "/vendor/autoload.php") ? require_once __DIR__ . "/vendor/autoload.php" : null;

$wp_dotenv = Dotenv\Dotenv::createImmutable([__DIR__ . '/../../../', __DIR__]); // wp root
$wp_dotenv->load();
!defined('ENV') ? define('ENV', getenv('PAGE_ENV')) : null;
include_once __SZKODA_KOORDYNATOR_DIR__ . "/inc/bootstrap.php";
