#! /bin/bash
# GPL 3 or newer

set -x
set -e
pwd
npm i

ls -alh /tmp

cp /tmp/assets-compile.sh /var/www/html/
#chown gaad /var/www/html/assets-compile.sh
#chgrp gaad /var/www/html/assets-compile.sh

cp /tmp/package.json /var/www/html/
#chown gaad /var/www/html/package.json
#chgrp gaad /var/www/html/package.json

cp -r /tmp/repo /var/www/html/
#chown -R gaad /var/www/html/repo/gassets-comp
#chgrp -R gaad /var/www/html/repo/gassets-comp

node /var/www/html/assets-compile.js