<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200718213530 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE pa_insurance_case (owner_id INT DEFAULT NULL, ID INT AUTO_INCREMENT NOT NULL, sessionID VARCHAR(32) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_97F1AB1D7E3C61F9 (owner_id), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");
        $this->addSql("CREATE TABLE pa_insurance_case2user (user_id INT DEFAULT NULL, insurance_case_id INT DEFAULT NULL, ID INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_3908DDA3A76ED395 (user_id), INDEX IDX_3908DDA32231F150 (insurance_case_id), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");
        $this->addSql("CREATE TABLE pa_insurance_case_meta (insurance_case_id INT DEFAULT NULL, ID INT AUTO_INCREMENT NOT NULL, meta_name VARCHAR(255) NOT NULL, meta_value LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_2CBAC232231F150 (insurance_case_id), PRIMARY KEY(ID)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
