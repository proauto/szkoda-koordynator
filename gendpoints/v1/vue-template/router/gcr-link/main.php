<?php
//this is gcr-link HTML template
?>
<div class="vue-component-gcr-link" @click="clickEvent($event)">
    <restricted-access v-if="restricted"
                       :ifFailed="ifFailed"
                       :fallbackComponent="fallbackComponent"
                       :msg="msg"
                       :needCaps="needCaps">
        <template v-slot:restricted-data>
            <slot></slot>
        </template>
    </restricted-access>
    <slot v-else></slot>
</div>
