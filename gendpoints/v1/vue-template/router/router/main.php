<?php
//this is router HTML template
?>
<div class="vue-component-router">
    {{component_}}
    <component v-if="component_ !== ''" :is="component_"></component>
</div>
