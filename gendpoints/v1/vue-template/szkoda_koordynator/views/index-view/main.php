<div class="vue-component-index">
    <workspace v-if="!dataLoading"></workspace>
    <loading v-else :loading-msg="true"></loading>
</div>