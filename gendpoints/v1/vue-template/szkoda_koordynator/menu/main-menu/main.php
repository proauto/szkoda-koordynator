<?php
//this is main-menu HTML template
?>
<div class="vue-component-main-menu">
    <ul>
        <li>
            <gcr-link :restricted="true"
                      :ifFailed="'hide'"
                      :needCaps="'index-view'"
                      :path="'/'">{{ $root.tr('Tasks') }}
            </gcr-link>
        </li>
    </ul>
</div>
