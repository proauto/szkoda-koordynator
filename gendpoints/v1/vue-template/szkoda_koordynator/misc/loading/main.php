<?php
//this is loading HTML template
?>
<div class="vue-component-loading">
    <i class="fas fa-spinner fa-pulse"></i>
    <p v-if="loadingMsg_ && msg !== ''" class="loading-msg">{{ msg }}</p>
</div>
