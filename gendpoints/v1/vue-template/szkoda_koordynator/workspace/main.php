<?php
//this is workspace HTML template
?>
<div :class="getClass()" v-if="!dataLoading">

    <h1>Workspace</h1>
    {{ general }}

</div>
<loading v-else :loading-msg="true"></loading>