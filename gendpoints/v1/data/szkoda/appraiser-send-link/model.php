<?php
//appraiser-send-link model

use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;
use Gaad\Gendpoints\Entity\User;

$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;
global $oGEEntityManager;

$caseID = (int)$this->getHeader('Caseid');

$aOutput = [];
$insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
$userRepository = $oGEEntityManager->getRepository(User::class);
$insuranceCase = $insuranceCaseRepository->findOneBy(["ID" => $caseID]);
$oCaseMetaRepository = $oGEEntityManager->getRepository(InsuranceCaseMeta::class);
if ($insuranceCase) {
    $caseMeta = $oCaseMetaRepository->findBy(['insuranceCase' => $insuranceCase->getId()]);
    $insuranceCase->setMeta($caseMeta);
    $insuranceCaseData = $insuranceCase->toArray();
    $aOutput['appraiserUrl'] = get_permalink(get_posts(['name' => __SZKODA_RZECZOZNAWCA_PAGE__, 'post_type' => 'page'])[0]->ID) . "?cid=" . $insuranceCase->getSessionID();
    $appraiser = $userRepository->findOneBy(["ID" => $insuranceCase->getOwner()]);

    //sending email
    $email = new insuranceCaseAppraiser();
    $email->trigger($insuranceCase, $appraiser);
}

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");