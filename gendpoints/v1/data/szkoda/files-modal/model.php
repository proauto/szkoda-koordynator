<?php
//files model
global $oGEEntityManager;

use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;
use Gaad\Gendpoints\GEndpoint;


$aHeaders = array_merge($this->getAllHeaders(), $this->getAHeaders());
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;

$filesFieldsList = [
    "insurance-carrier-calculation"=>true,
    "file-custom-1"=>true,
    "file-custom-2"=>true,
    "file-custom-3"=>true,
    "file-custom-4"=>true,
    "file-custom-5"=>true
];
$caseID = (int)$this->getHeader( 'Caseid');
$insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
$insuranceCase = $insuranceCaseRepository->findOneBy(["ID" => $caseID]);
$oCaseMetaRepository = $oGEEntityManager->getRepository(InsuranceCaseMeta::class);
if ($insuranceCase){
    $caseMeta = $oCaseMetaRepository->findBy(['insuranceCase' => $insuranceCase->getId()]);
    $insuranceCase->setMeta($caseMeta);
    $insuranceCaseData = $insuranceCase->toArray();
}
function getURI( $path ):string{
    return "https://". implode("/",[get_bloginfo(), "wp-content", explode("wp-content/", $path)[1]]);
}

if ($insuranceCaseData){
    $filesFields = array_intersect_key($insuranceCaseData,$filesFieldsList);
    $files = array_filter($filesFields, function($var){
        return is_file($var);
    }, ARRAY_FILTER_USE_BOTH);
    foreach( $files as $name => $path) $files [$name] = getURI($path);
}

$aOutput = [
    "files" => $files
];
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");