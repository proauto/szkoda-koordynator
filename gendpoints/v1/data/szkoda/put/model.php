<?php
//put model


use Gaad\Gendpoints\Model\User;
use Gaad\SzkodaKoordynator\Handlers\AccessManager;
use Gaad\SzkodaKoordynator\Handlers\SzkodaRecordManager;
use Gaad\SzkodaKoordynator\Handlers\szkodaRecord as szkodaRecord;
global $oGEEntityManager;
$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;

$oRecord = new szkodaRecord($_POST['sessionID'], $_FILES['bundleZipFile']);
$oRecord->processRecord();

$oUserRepository = $oGEEntityManager->getRepository(\Gaad\Gendpoints\Entity\User::class);
$currentUser = $oUserRepository->findOneBy(["ID" => 1]);

$rzeczoznawcaMng = new SzkodaRecordManager(new AccessManager($currentUser));
$rzeczoznawcaMng->setOwner($oRecord);


do_action("paszko_new_case_client", $oRecord->getInsuranceCase());
do_action("paszko_new_case_appraiser", $oRecord->getInsuranceCase(), $rzeczoznawcaMng->getMasterAppraiser());
do_action("paszko_new_case_manager", $oRecord->getInsuranceCase(), $rzeczoznawcaMng->getManager());



$r=1;
$aOutput = [
    'data' => isset($_POST['sessionID']),
    'attachment' => isset($_FILES['bundleZipFile'])
];
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");