<?php
//accept model

use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;

global $oGEEntityManager;
$sessionID = $this->getHeader('Sessionid');
$calculationID = $this->getHeader('Calculationid');
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;
$aOutput = [];
$currentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $currentUser->decodeToken($token);

if ($decoded) {
    $userID = $decoded->userID;
    $criteria = ['sessionID' => $sessionID];
    $wpUser = new WP_User($userID);

    if ($wpUser->has_cap('rzeczoznawca')) $criteria['owner_id'] = $userID;
    $InsuranceCaseCalculationRepository = $oGEEntityManager->getRepository(InsuranceCaseCalculation::class);
    $InsuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
    $insuranceCase = $InsuranceCaseRepository->findOneBy($criteria);
    $calculation = $InsuranceCaseCalculationRepository->findOneBy(['ID' => $calculationID]);
    if ($insuranceCase instanceof InsuranceCase && $calculation instanceof InsuranceCaseCalculation) {
        $insuranceCase->setStatus(InsuranceCase::STATUS_SENT);
        $insuranceCase->setCalculation($calculation);
        $oGEEntityManager->persist($insuranceCase);
        $oGEEntityManager->flush();
    } else {
        $r=1; //@todo error throw here
    }
}

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");