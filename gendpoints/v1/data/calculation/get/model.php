<?php
//calculation/get model
global $oGEEntityManager;

use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;

$this->getHeader = $this->getAllHeaders();
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$hash = $this->getHeader('Hash');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;
$aOutput = [];

$currentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $currentUser->decodeToken($token);
if($decoded){
    $userID = $decoded->userID;
    $InsuranceCaseCalculationRepository = $oGEEntityManager->getRepository(InsuranceCaseCalculation::class);
    $calculation = $InsuranceCaseCalculationRepository->findOneBy([
        'hash' => $hash,
        ]);
    if($calculation instanceof InsuranceCaseCalculation){


        $aOutput['calculation'] = [
            'id'=>$calculation->getID(),
            'value'=>$calculation->getValue(),
            'margin'=>$calculation->getValueMargin(),
            'client'=>$calculation->getValueClient()
        ];
    }
}

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");