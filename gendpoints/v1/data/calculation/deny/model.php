<?php
//deny model

global $oGEEntityManager;

use Gaad\Gendpoints\Entity\InsuranceCase;

$sessionID = $this->getHeader('Sessionid');
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;

$aOutput = [];

$currentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $currentUser->decodeToken($token);
if ($decoded) {
    $userID = $decoded->userID;
    $criteria = ['sessionID' => $sessionID];
    $wpUser = new WP_User($userID);

    if ($wpUser->has_cap('rzeczoznawca')) $criteria['owner_id'] = $userID;
    $InsuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
    $calculation = $InsuranceCaseRepository->findOneBy($criteria);
    if ($calculation instanceof InsuranceCase) {
        $calculation->setStatus(InsuranceCase::STATUS_DENIED);
        $oGEEntityManager->persist($calculation);
        $oGEEntityManager->flush();
    } else {
        $r=1; //@todo error throw here
    }
}
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");