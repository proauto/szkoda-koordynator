<?php
//update model
global $oGEEntityManager;

use Cassandra\Date;
use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;

$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;
$aOutput = [];
$encodedData = json_decode(urldecode(base64_decode(trim($this->getHeader('encodeddata')))), true);
$this->setAHeaders(array_merge($this->getAHeaders(), $encodedData));
$newMeta = [
    "manager-case-description" => "manager-case-description",
];
$metaWhiteList = array_merge($newMeta, [
    "case-status" => "case-status",
    "client-full-name" => "Client-Full-Name",
    "client-phone-number" => "Client-Phone-Number",
    "client-email" => "Client-Email",
    "insurance-type" => "Insurance-Type",
    "collision-in-pl" => "Collision-In-Pl"
]);

$currentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $currentUser->decodeToken($token);
if ($decoded) {
    $userID = $decoded->userID;
    $insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
    $insuranceCaseMetaRepository = $oGEEntityManager->getRepository(InsuranceCaseMeta::class);
    $insuranceCaseCalculationRepository = $oGEEntityManager->getRepository(InsuranceCaseCalculation::class);
    $calculation = $insuranceCaseCalculationRepository->findOneBy(["ID" => $this->getHeader('Caid')]);
    $insuranceCase = $insuranceCaseRepository->findOneBy(["sessionID" => $this->getHeader('Cid')]);
    if ($insuranceCase) {

        /** @var array TYPE_NAME $insuranceCaseMeta */
        $insuranceCaseMeta = $insuranceCaseMetaRepository->findBy(["insuranceCase" => $insuranceCase]);
        $calculation ? $insuranceCase->setCalculation($calculation) : null;
        $insuranceCase->setModifiedAt();
        $insuranceCase->setOwner($this->getHeader('Case-Owner'));
        $insuranceCase->setStatus($this->getHeader('Case-Status'));
        $oGEEntityManager->persist($insuranceCase);

        if ($insuranceCaseMeta) {
            foreach ($insuranceCaseMeta as $meta) {
                $mapped = $metaWhiteList[$meta->getMetaName()];
                if (isset($mapped)) {
                    $meta->setMetaValue($this->getHeader($mapped));
                    $oGEEntityManager->persist($meta);
                    if (isset($newMeta[$meta->getMetaName()])) unset($newMeta[$meta->getMetaName()]);
                }
            }
        }

        if (!empty($newMeta)) {
            foreach ($newMeta as $k => $v) {
                $meta = new InsuranceCaseMeta();
                $meta->setInsuranceCase($insuranceCase);
                $meta->setMetaName($k);
                $meta->setMetaValue($this->getHeader($k));
                $oGEEntityManager->persist($meta);
            }
        }

        $oGEEntityManager->flush();

    }
}
$aOutput['modified'] = new DateTime();

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");