<?php
//remove model

use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;

$sessionID = $this->getHeader('Sessionid');
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;

$aOutput = [];

$currentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $currentUser->decodeToken($token);
if ($decoded) {
    global $oGEEntityManager;
    $userID = $decoded->userID;
    $insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
    $insuranceCaseMetaRepository = $oGEEntityManager->getRepository(InsuranceCaseMeta::class);
    $insuranceCaseCalculationRepository = $oGEEntityManager->getRepository(InsuranceCaseCalculation::class);
    /** @var array insuranceCase $insuranceCase */
    $insuranceCase = $insuranceCaseRepository->findOneBy(["ID" => $this->getHeader('Cid')]);
    if ($insuranceCase) {
        /** @var array InsuranceCaseCalculation $calculation */
        $calculation = $insuranceCaseCalculationRepository->findBy(["insuranceCase" => $insuranceCase]);
        /** @var array insuranceCaseMeta $InsuranceCaseCalculation */
        $insuranceCaseMeta = $insuranceCaseMetaRepository->findBy(["insuranceCase" => $insuranceCase]);


        $oGEEntityManager->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($calculation as $c) $oGEEntityManager->remove($c);
        foreach ($insuranceCaseMeta as $m) $oGEEntityManager->remove($m);
        $oGEEntityManager->remove($insuranceCase);
        $oGEEntityManager->flush();
        $oGEEntityManager->getConnection()->exec('SET FOREIGN_KEY_CHECKS = 1;');
    }

}
$r = 1;


$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");