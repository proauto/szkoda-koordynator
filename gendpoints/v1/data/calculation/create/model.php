<?php
//create model

use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\User;
use Gaad\SzkodaKoordynator\Handlers\InsuranceCaseCalculationManager;
use Gaad\SzkodaKoordynator\Handlers\ProAutoInsuranceCaseCalculator;

global $oGEEntityManager;
$headers = $this->getAllHeaders();
$value = $this->getHeader('Value');
$sessionID = $this->getHeader('Cid');
$description = $this->getHeader('Description');
$cuID = $this->getHeader('Cuid');
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;
$currentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $currentUser->decodeToken($token);
$aOutput = [];

if ($decoded) {
    $userID = $decoded->userID;
    $wpUser = new WP_User($userID);

    $insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
    $insuranceCase = $insuranceCaseRepository->findOneBy(["sessionID" => $sessionID]);
    $userRepository = $oGEEntityManager->getRepository(User::class);
    $owner = $userRepository->findOneBy(["ID" => $userID]);
    if ($insuranceCase instanceof InsuranceCase) {
        $calculationManager = new InsuranceCaseCalculationManager($insuranceCase, new ProAutoInsuranceCaseCalculator());
        $calculationManager->createCalculation($value);
        $calculationManager->setDescription($description);
        $calculationManager->setInsuranceCase($insuranceCase);
        if($owner instanceof User) $calculationManager->setOwner($owner);
        $success = $calculationManager->saveCalculation();
        $aOutput['calculation'] = $calculationManager->getCalculation()->toArray();
    }

}
$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");