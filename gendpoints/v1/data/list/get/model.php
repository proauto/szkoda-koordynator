<?php
//get model

use Gaad\Gendpoints\Entity\User;
use Gaad\SzkodaKoordynator\Handlers\AccessManager;
use Gaad\SzkodaKoordynator\Handlers\SzkodaRecordManager;

global $oGEEntityManager;

$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;

$oCurrentUser = new \Gaad\Gendpoints\Model\User();
$token = str_replace("Bearer ", "", (string)$this->getHeader("Authorization"));
$decoded = $oCurrentUser->decodeToken($token);

$oUserRepository = $oGEEntityManager->getRepository(\Gaad\Gendpoints\Entity\User::class);
$currentUser = $oUserRepository->findOneBy(["ID" => $decoded->userID]);

if(!is_null($currentUser))// $currentUser= $oUserRepository->findOneBy([ "ID" => 1 ]);
{
    $szkodaRecordMng = new SzkodaRecordManager(new AccessManager($currentUser));

    $data = $szkodaRecordMng->getTheList();
    foreach ($data as $k => $record) $this->oModel->addData($record, $k);
}

