<?php
//send model
require_once get_stylesheet_directory() . "/woocommerce/emails/insuranceCaseClient.php";
$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;
$aOutput = [];

do_action("paszko_new_case_client", new WP_User(1));
do_action("paszko_new_case_appraiser", new WP_User(1));
do_action("paszko_new_case_manager", new WP_User(1));
do_action("paszko_new_calculation_client", new WP_User(1));
do_action("paszko_new_calculation_manager", new WP_User(1));
do_action("paszko_case_denied_client", new WP_User(1));
do_action("paszko_case_denied_manager", new WP_User(1));
$R=1;

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");