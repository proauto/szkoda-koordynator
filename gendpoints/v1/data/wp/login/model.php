<?php
//login model

$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $this->getHeader('Ge-Data-Origin');
$iCacheAlive = $this->getHeader('Cache-Alive') ? (int)$this->getHeader('Cache-Alive') : 600;
$encodedData = json_decode(urldecode(base64_decode(trim($this->getHeader('encodeddata')))), true);
if(!empty($encodedData)){
    $username = $encodedData['l'];
    $password = $encodedData['p'];
    $remember = $encodedData['r'];
}

$aOutput = [];
$creds = [
    'user_login' => $username,
    'user_password' => $password,
    'remember' => (bool)$remember
];
$user = wp_signon($creds, FALSE);
if (is_wp_error($user)) {
    $aOutput = [
        'success' => FALSE
    ];
    foreach ($user->errors as $errorName => $data) {
        $aOutput['error'] = $errorName;
        break;
    }
} else {
    wp_set_auth_cookie($user->ID, $creds['remember']);
    $aOutput = [
        'ua' => $user->ID,
        'success' => $user->allcaps['edit_products']
    ];
}

$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");