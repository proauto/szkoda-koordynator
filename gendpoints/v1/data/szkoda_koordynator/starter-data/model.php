<?php
//starter-data model
use Gaad\Gendpoints\GEndpoint;

$aHeaders = $this->getAllHeaders();
$sGeDataOrigin = $aHeaders['Ge-Data-Origin'];
$iCacheAlive = isset($aHeaders['Cache-Alive']) ? (int)$aHeaders['Cache-Alive'] : 600;

$aOutput = [
    "starter-data" => 1
];
$sOutput = json_encode($aOutput);
$sOutput = urldecode($sOutput);
$aOutput = json_decode($sOutput, true);


$this->oModel->addData($sGeDataOrigin, "Ge-Data-Origin");
$this->oModel->addData($iCacheAlive, 'Cache-Alive');
$this->oModel->addData($aOutput, "Module-Data");