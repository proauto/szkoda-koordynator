window['workspaceComponentObject'] = Vue.component('workspace', {
    template: '#szkoda_koordynator-template-workspace',
    data: function () {
        return {
            dataLoading: this.$root.dataLoading,
            general: {},
            leftPanelOpened: false,
            rightPanelOpened: false
        }
    },

    beforeMount: function () {

    },

    created: function () {
        this.dataUpdate();
        this.$root.bus.$on('general-updated', this.dataUpdate);
    },

    methods: {
        panelToggle: function ($event, variableName) {
            if (undefined !== this[variableName] && typeof this[variableName] === "boolean") {
                this[variableName] = !this[variableName];
            }
        },
        getClass: function () {

            var classObj = {
                dataLoading: this.dataLoading,
                'vue-component-workspace': true,
                'left-panel-opened': this.leftPanelOpened,
                'right-panel-opened': this.rightPanelOpened
            };

            if (typeof this.classString !== 'undefined') {
                classObj[this.classString] = true;
            }

            return classObj;
        },

        dataUpdate: function () {

            if (undefined !== this.$store.state.generalModule) {
                this.general = this.$store.state.generalModule;
            }

            $workspace = this;
            $app = window[window.geConfig.szkoda_koordynator.appVarName];

            setTimeout(function () {
                if (typeof $workspace !== "undefined" && typeof $workspace.$store !== "undefined") {
                    $workspace.general = $app.$store.state.generalModule;
                    $workspace.dataLoading = false;
                    $app.appDataLoadingOff();
                }
                delete $workspace;
                delete $app;
            }, 5);
        }
    }
});
