window['notificationComponentObject'] = Vue.component('notification', {
    template: '#szkoda_koordynator-template-notification',
    props: ['type', 'msg'],
    data: function () {
        return {}
    },

    methods: {}
});