<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;

define('DB_NAME', 'shu3va_pamanage');
define('DB_USER', 'ocug9g_pamanage');
define('DB_PASSWORD', 'hahLaubah7Il');
define('DB_HOST', 'pamanager-stage.grupa-nova.pl.mysql.dhosting.pl');
define('ENV', 'dev');
define('__SZKODA_KOORDYNATOR_DIR__', __DIR__ . "/../");

$config = Setup::createAnnotationMetadataConfiguration(
    [
        dirname(__SZKODA_KOORDYNATOR_DIR__) . "/../../gendpoints2/inc/class/Entity",
        __SZKODA_KOORDYNATOR_DIR__ . "inc/class/Entity"
    ],
    ENV === 'dev' ?? false, null, null, false);


$proxyTmpDir = explode("/wp-content", dirname(__FILE__))[0] . "/.proxy-tmp";
if (!is_dir($proxyTmpDir)) @mkdir($proxyTmpDir);
if (is_dir($proxyTmpDir))
    $config->setProxyDir($proxyTmpDir);

$oGAEntityManager = EntityManager::create([
    'driver' => 'pdo_mysql',
    'host' => DB_HOST,
    'user' => DB_USER,
    'password' => DB_PASSWORD,
    'dbname' => DB_NAME
], $config);

$oGAEntityManager
    ->getConnection()
    ->getDatabasePlatform()
    ->registerDoctrineTypeMapping('enum', 'string');

return ConsoleRunner::createHelperSet($oGAEntityManager);
