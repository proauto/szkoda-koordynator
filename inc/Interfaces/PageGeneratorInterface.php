<?php

namespace Gaad\Ganer\Interfaces;


if (!interface_exists(__NAMESPACE__ . '\PageGeneratorInterface')) {
    interface PageGeneratorInterface
    {
        public function render();
    }
}

