<?php

namespace Gaad\Ganer\Interfaces;


use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;

if (!interface_exists(__NAMESPACE__ . '\InsuranceCaseCalculator')) {
    interface InsuranceCaseCalculator
    {
        /**
         * @param InsuranceCase|null $insuranceCase
         * @param int $value
         * @return array
         */
        public function calculate(?InsuranceCase $insuranceCase, int $value);

        /**
         * @param InsuranceCaseCalculation $insuranceCase
         * @return mixed
         */
        public function applyCalculation(InsuranceCaseCalculation $insuranceCase);

        /**
         * @return string
         */
        public function getEquationID(): string;
    }
}

