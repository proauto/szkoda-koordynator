window['{{componentName}}ComponentObject'] = Vue.component('{{componentName}}', {
template: '#szkoda_koordynator-template-{{componentName}}',
data: function () {
var storeModuleName = '{{componentName}}-view'.camelize();
return {
dataLoading: this.$root.dataLoading,
storeModuleName: storeModuleName,
storageType: 'sessionStorage'
}
},

beforeCreate: function () {
var cacheDisabled = geConfig.szkoda_koordynator.sessionStorageCacheDisabled;
if (!cacheDisabled) {
var LocStorStats_ = new LocStorStats(geConfig.szkoda_koordynator.sessionStorageCollectionName || 'gc', 'sessionStorage');
LocStorStats_.clearCache();
}
},

created: function () {
this.$root.bus.$on('dataLoadingDone', this.dataLoadingDone);
this.$root.bus.$on('cache-cleared', this.cacheClearedCallback);
},

watch: {
"$root.dataLoading": function () {
this.dataLoading = this.$root.dataLoading;
}
},

mounted: function () {
this.taskID = this.$root.getRouteParam('id');

if (this.$root.dataLoadedIndex.indexOf(this.storeModuleName) === -1) {
this.$root.appDataLoadingOn();
this.dataLoading = true;
this.storeModuleName ?
this.$root.enqueueApiData(new apiDataRequest(this.storeModuleName, {
type: 'post',
loadingMsg: "Loading notes",
alive: this.$root.getModuleCacheAlive('{{componentName}}-view'.camelize()),
endpoint: this.$root.getModuleEndpoint('{{componentName}}-view'.camelize()),
headers: {
Id: this.taskID
}
}, this.$root)) : false;
} else {
this.$root.appDataLoadingOff();
this.dataLoading = false;
}
},

methods: {
cacheClearedCallback: function (payload) {
this.$root.cacheClearedCallback(payload, geConfig.szkoda_koordynator[this.storageType + 'CollectionName'] + '_' + this.storeModuleName);
},

dataLoadingDone: function () {
this.dataLoading = false;
}
}
});