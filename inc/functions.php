<?php

namespace Gaad\SzkodaKoordynator\Core\Filters;

use Gaad\Gendpoints\Config\Config;
use Gaad\Gendpoints\Entity\Option;
use Gaad\Gendpoints\Entity\Post;
use Gaad\Gendpoints\Entity\User;
use Gaad\SzkodaKoordynator\Forms\caseDetailsEditAdminContactForm7Form;
use Gaad\SzkodaKoordynator\Forms\caseDetailsEditManagerContactForm7Form;
use Gaad\SzkodaKoordynator\Forms\signInContactForm7Form;
use Gaad\SzkodaKoordynator\Handlers\AccessManager;
use Gaad\SzkodaKoordynator\Handlers\KoordynatorViewController;
use WP_User;


// Monvita Statusy
function status_post_type() {

    $labels = array(
        'name'                  => _x( 'Statusy', 'Post Type General Name', 'text_domain' ),
        'singular_name'         => _x( 'Statusy', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'             => __( 'Statusy', 'text_domain' ),
        'name_admin_bar'        => __( 'Statusy', 'text_domain' ),
        'archives'              => __( 'Item Archives', 'text_domain' ),
        'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
        'all_items'             => __( 'Lista', 'text_domain' ),
        'add_new_item'          => __( 'Add New Item', 'text_domain' ),
        'add_new'               => __( 'Dodaj status', 'text_domain' ),
        'new_item'              => __( 'Nowy', 'text_domain' ),
        'edit_item'             => __( 'Edytu', 'text_domain' ),
        'update_item'           => __( 'Update Item', 'text_domain' ),
        'view_item'             => __( 'View Item', 'text_domain' ),
        'search_items'          => __( 'Search Item', 'text_domain' ),
        'not_found'             => __( 'Not found', 'text_domain' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
        'featured_image'        => __( 'Featured Image', 'text_domain' ),
        'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
        'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
        'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
        'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
        'items_list'            => __( 'Items list', 'text_domain' ),
        'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
        'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
    );
    $args = array(
        'label'                 => __( 'Statusy', 'text_domain' ),
        'description'           => __( 'Statusy spraw', 'text_domain' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'taxonomies' 			=> [],
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'show_in_admin_bar'     => false,
        'show_in_nav_menus'     => false,
        'can_export'            => false,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'status', $args );
}

function loadTranslations(){
    $mo = __SZKODA_KOORDYNATOR_DIR__ . '/szkoda_koordynator-pl_PL.mo';
    if(is_file($mo )){
        \load_textdomain('szkoda_koordynator', $mo );
    }
}


add_action("init", "\Gaad\SzkodaKoordynator\Core\Filters\loadTranslations");
add_action("init", "\Gaad\SzkodaKoordynator\Core\Filters\status_post_type");


function wp_redirect_manager_page()
{
    if (is_admin()) return;
    wp_redirect('https://' . get_bloginfo() . '/' . __SZKODA_KOORDYNATOR_PAGE__, 301);
    exit;
}

function redirect_to_manager_page($wp)
{
    global $post;

    if (is_page(__SZKODA_RZECZOZNAWCA_PAGE__) || $post->post_type === 'post'){
        return;
    }

    if ((!is_page(__SZKODA_KOORDYNATOR_PAGE__)) && !is_user_logged_in()) {
        wp_redirect_manager_page();
    }

    if (is_user_logged_in()) {
        $userID = get_current_user_id();
        if (0 !== $userID) {
            $user = new \WP_User($userID);

            if (
                (
                    $user->has_cap('administrator')
                    || $user->has_cap('rzeczoznawca')
                ) && is_page(__SZKODA_RZECZOZNAWCA_PAGE__))
            {
                return; //pass trough to render case page
            }

            if (
                (
                    $user->has_cap('koordynator')
                    || $user->has_cap('administrator')
                )
                && !is_page(__SZKODA_KOORDYNATOR_PAGE__)) {
                wp_redirect_manager_page();
            }

            if (
                $user->has_cap('rzeczoznawca')
                && !is_page(__SZKODA_RZECZOZNAWCA_PAGE__)) {
                return;
            }
        }

    } else {


        if (!is_page(__SZKODA_KOORDYNATOR_PAGE__)) {
            wp_redirect_manager_page();
        }
    }

}


$caseDetailsEditAdminContactForm7Form = new caseDetailsEditAdminContactForm7Form();
$caseDetailsEditManagerContactForm7Form = new caseDetailsEditManagerContactForm7Form();
$signInContactForm = new signInContactForm7Form();

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\viewControllerInitialize')) {
    function viewControllerInitialize()
    {

        global $oGEEntityManager;
        $oUserRepository = $oGEEntityManager->getRepository(User::class);
        $currentUser = $oUserRepository->findOneBy(["ID" => get_current_user_id()]);
        if ($currentUser instanceof User) new KoordynatorViewController(new AccessManager($currentUser));
        else {
            $R = 1;
        }
    }
}


if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\getOptionValue')) {
    function getOptionValue(string $sName)
    {
        global $oGAEntityManager;
        $optionsRepository = $oGAEntityManager->getRepository(Option::class);
        $oOption = $optionsRepository->findOneBy(['option_name' => $sName]);
        return $oOption ? $oOption->getOptionValue() : "";
    }
}

if (!function_exists('\Gaad\PaSzkodaWidget\Core\Filters\defaultSystemElementsInstaller')) {
    function defaultSystemElementsInstaller()
    {
        global $oGAEntityManager;

        $postsRepository = $oGAEntityManager->getRepository(Post::class);
        $optionsRepository = $oGAEntityManager->getRepository(Option::class);
        $systemElementsInitialized = $optionsRepository->findOneBy(['option_name' => __SZKODA_KOORDYNATOR_WIDGET_SYSTEM_ELEMENTS_INIT__]);
        if (!$systemElementsInitialized) {
            $oKoordynatorPage = $postsRepository->findOneBy(
                [
                    'post_name' => __SZKODA_KOORDYNATOR_PAGE__,
                    'post_status' => "publish",
                    'post_type' => "page",
                ]
            );

            if (is_null($oKoordynatorPage)) {
                $iKoordynatorPageID = wp_insert_post(
                    array(
                        'comment_status' => 'close',
                        'ping_status' => 'close',
                        'post_author' => 1,
                        'post_title' => ucwords(__SZKODA_KOORDYNATOR_PAGE__),
                        'post_name' => strtolower(str_replace(' ', '-', trim(__SZKODA_KOORDYNATOR_PAGE__))),
                        'post_status' => 'publish',
                        'post_content' => 'Koordynator admin panel content html',
                        'post_type' => 'page',
                        'post_parent' => ''
                    )
                );
            }

            $oRzeczoznawcaPage = $postsRepository->findOneBy(
                [
                    'post_name' => __SZKODA_RZECZOZNAWCA_PAGE__,
                    'post_status' => "publish",
                    'post_type' => "page",
                ]
            );

            if (is_null($oRzeczoznawcaPage)) {
                $iRzeczoznawcaPageID = wp_insert_post(
                    array(
                        'comment_status' => 'close',
                        'ping_status' => 'close',
                        'post_author' => 1,
                        'post_title' => ucwords(__SZKODA_RZECZOZNAWCA_PAGE__),
                        'post_name' => strtolower(str_replace(' ', '-', trim(__SZKODA_RZECZOZNAWCA_PAGE__))),
                        'post_status' => 'publish',
                        'post_content' => 'Rzeczoznawca admin panel content html',
                        'post_type' => 'page',
                        'post_parent' => ''
                    )
                );
            }


            $oOption = new Option();
            $oOption->setOptionName(__SZKODA_KOORDYNATOR_WIDGET_SYSTEM_ELEMENTS_INIT__);
            $oOption->setOptionValue(true);
            $oOption->setAutoload(false);
            //      $oGAEntityManager->persist($oOption);
        }


    }
}
if (!function_exists('\Gaad\PaSzkodaWidget\Core\Filters\defaultOptionsInstaller')) {

    /**
     * Installs essential options values to database on first plugin run
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    function defaultOptionsInstaller()
    {
        global $oGAEntityManager;

        $aOptionsDefault = [
            __SZKODA_KOORDYNATOR_WIDGET_OPTIONS_INIT__ => true
        ];
        $optionsRepository = $oGAEntityManager->getRepository(Option::class);
        $optionsInitialized = $optionsRepository->findOneBy(['option_name' => __SZKODA_KOORDYNATOR_WIDGET_OPTIONS_INIT__]);
        if (!$optionsInitialized) {
            foreach ($aOptionsDefault as $sName => $value) {
                $oOption = new Option();
                $oOption->setOptionName($sName);
                $oOption->setOptionValue($value);
                $oOption->setAutoload(false);
                $oGAEntityManager->persist($oOption);
            }
            $oGAEntityManager->flush();
        }
    }
}

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\vueAppScriptDefer')) {
    function vueAppScriptDefer($tag, $handle, $src)
    {
        if (in_array($handle, ['vue-app'])) {
            return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
        }
        return $tag;
    }
}

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\vueTemplatesLoading')) {
    function vueTemplatesLoading()
    {
        global $oSZKODA_KOORDYNATORVueComponents;
        $oSZKODA_KOORDYNATORVueComponents->render();
    }
}

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\buildStylesQueue')) {
    function buildStylesQueue()
    {
        wp_enqueue_style('1bootstrap-css', "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css");
        //wp_enqueue_style('normalize-css', "https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css.map");
        wp_enqueue_style('datatables-custom-css', 'https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.21/b-1.6.2/b-html5-1.6.2/cr-1.5.2/fc-3.3.1/fh-3.1.7/kt-2.5.2/r-2.2.5/rr-1.2.7/sc-2.0.2/sp-1.1.1/datatables.min.css', ['bootstrap-css'], FALSE, TRUE);

    }
}

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\buildAssetsQueue')) {
    function buildAssetsQueue()
    {
        global $post;
        wp_enqueue_script('vue-app-szkoda_koordynator-routes', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/routes.js', [], FALSE, TRUE);
        wp_enqueue_script('axios', 'https://unpkg.com/axios/dist/axios.min.js', FALSE, FALSE, TRUE);
        wp_enqueue_script('vue', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js', FALSE, FALSE, TRUE);
        wp_enqueue_script('vue-x', 'https://unpkg.com/vuex', ['vue'], FALSE, TRUE);
        wp_enqueue_script('vue-router', 'https://unpkg.com/vue-router/dist/vue-router.js', ['vue'], FALSE, TRUE);
        wp_enqueue_script('jquery-slim', 'https://code.jquery.com/jquery-3.3.1.js', [], FALSE, TRUE);
        wp_enqueue_script('popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', ['jquery-slim'], FALSE, TRUE);
        wp_enqueue_script('bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', ['jquery-slim', 'popper-js'], FALSE, TRUE);
        wp_enqueue_script('font-awesome-js', 'https://kit.fontawesome.com/383cbb7a03.js', [], FALSE, TRUE);
        wp_enqueue_script('ge-tinymce', 'https://cdn.tiny.cloud/1/e5q7q5lxxbzonp9x3egobpy366ozh7srkmbs3ica42xqd9yy/tinymce/5/tinymce.min.js', [], FALSE, FALSE);
        wp_enqueue_script('sortable-js', '//cdn.jsdelivr.net/npm/sortablejs@1.8.4/Sortable.min.js', [], FALSE, TRUE);
        wp_enqueue_script('vue-draggable-js', '//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.20.0/vuedraggable.umd.min.js', ['vue'], FALSE, TRUE);
        wp_enqueue_script('vue-app-szkoda_koordynator-store', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/store.js', ['vue'], FALSE, TRUE);
        wp_enqueue_script('vue-validators', __SZKODA_KOORDYNATOR_URI__ . '/node_modules/vuelidate/dist/validators.min.js', [], FALSE, FALSE);
        wp_enqueue_script('vue-vuelidate', __SZKODA_KOORDYNATOR_URI__ . '/node_modules/vuelidate/dist/vuelidate.min.js', ['vue-validators'], FALSE, TRUE);
        wp_enqueue_script('vue-app-szkoda_koordynator-translations', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/app-translations-en.js', false, FALSE, TRUE);
        wp_enqueue_script('vue-app', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/app.js', ['vue-app-szkoda_koordynator-translations', 'vue', 'vue-app-szkoda_koordynator-store', 'vue-app-szkoda_koordynator-router', 'vue-vuelidate'], FALSE, TRUE);
        wp_enqueue_script('api-data-request', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/api-data-request.js', [], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-item', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/loc-storage-item.js', [], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-mng', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/loc-storage-mng.js', ['app-loc-storage-stats', 'app-loc-storage-item'], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-preload', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/loc-storage-preload.js', ['app-loc-storage-stats', 'app-loc-storage-item'], FALSE, TRUE);
        wp_enqueue_script('app-loc-storage-stats', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/loc-storage-stats.js', [], FALSE, TRUE);
        wp_enqueue_script('fonts-awesome-kit', 'https://kit.fontawesome.com/383cbb7a03.js', [], FALSE, TRUE);

        //datatables
        wp_enqueue_script('pdf-make-js', 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js', [], FALSE, TRUE);
        wp_enqueue_script('pdf-make-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js', [], FALSE, TRUE);
        wp_enqueue_script('datatables-js', 'https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.21/b-1.6.2/b-html5-1.6.2/cr-1.5.2/fc-3.3.1/fh-3.1.7/kt-2.5.2/r-2.2.5/rr-1.2.7/sc-2.0.2/sp-1.1.1/datatables.min.js', ['jquery'], FALSE, TRUE);
        wp_enqueue_script('pa-scripts-js', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/pa-scripts.js', ['jquery'], FALSE, TRUE);
        if ("manager" === $post->post_name)
            wp_enqueue_script('datatables-integration-js', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/datatables-integration.js', ['datatables-js'], FALSE, TRUE);
        if ("appraiser" === $post->post_name)
            wp_enqueue_script('appraiser-integration-js', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/appraiser-integration.js', ['datatables-js'], FALSE, TRUE);

        wp_enqueue_script('wp-util');

        //Backend transport
        wp_enqueue_script('pa-api-data-request', __SZKODA_KOORDYNATOR_URI__ . '/assets/js/pa-api-data-request.js', [], FALSE, TRUE);
        $geConfig = new Config('general.yaml', $GLOBALS['aSZKODA_KOORDYNATORConfigDirectories']);

        $appraisers = get_users(['role' => 'rzeczoznawca']);
        $rest = json_decode($geConfig->getJSON(), true)['rest'];
        unset($rest['jwtSecretKey']);
        $user = new WP_User (get_current_user_id());
        wp_localize_script('axios', 'geConfig', array(
                'rest' => $rest,
                'appraisers' => $appraisers,
                'caps' => $user->get_role_caps(),
                'apiAccess' => [
                    'uid' => get_current_user_id(),
                    'domain' => (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST']
                ]
            )
        );

    }
}

/*
 * Adds necessary variables to url during invoking Ganner client admin panel options page
 */
if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\szkoda_koordynatorOptionsPageRedirect')) {
    function szkoda_koordynatorOptionsPageRedirect()
    {
        global $geConfig;
        $sOptionsPageSlug = $geConfig->get('optionsPageSlug', 'szkoda_koordynator');
        $sRouteVarName = $geConfig->get('routeVarName', 'szkoda_koordynator');
        if (isset($_GET['page']) && $sOptionsPageSlug === $_GET['page']) {
            if (!isset($_GET[$sRouteVarName])) {
                $sLocation = '//' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] . '&' . $sRouteVarName . '=/';
                header("Location: {$sLocation}");
            }
        }
    }
}


if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\userRolesAdd')) {
    function userRolesAdd()
    {
        global $geConfig;
        $aRolesYaml = $geConfig->get('roles-yaml', 'szkoda_koordynator-wp-roles');
        $oWPRoles = new Config(basename($aRolesYaml), [__SZKODA_KOORDYNATOR_DIR__ . "/config"]);

        foreach ($oWPRoles->get() as $slug => $roleMeta) {

            $role = add_role($slug, __($roleMeta['name'], ''),
                [
                    'read' => true, // true allows this capability
                    'edit_posts' => true, // Allows user to edit their own posts
                    'edit_pages' => false, // Allows user to edit pages
                    'edit_others_posts' => false, // Allows user to edit others posts not just their own
                    'create_posts' => false, // Allows user to create new posts
                    'manage_categories' => false, // Allows user to manage post categories
                    'publish_posts' => false, // Allows the user to publish, otherwise posts stays in draft mode
                    'edit_themes' => false, // false denies this capability. User can’t edit your theme
                    'install_plugins' => false, // User cant add new plugins
                    'update_plugin' => false, // User can’t update any plugins
                    'update_core' => false // user cant perform core updates
                ]
            );
            if ($role instanceof \WP_Role) {
                $role->add_cap('manage_options', true);
                if (array_key_exists('caps', $roleMeta) && !empty($roleMeta['caps'])) {
                    foreach ($roleMeta['caps'] as $capName) {
                        $role->add_cap($capName, true);
                    }
                }
            }
        }

    }
}

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\configDirectories')) {
    function configDirectories($input)
    {
        $input[] = __SZKODA_KOORDYNATOR_DIR__ . '/config';
        return $input;
    }
}

if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\endpointsDirectories')) {
    function endpointsDirectories($input)
    {
        $input[] = __SZKODA_KOORDYNATOR_DIR__ . '/gendpoints';
        return $input;
    }
}


/*
 * Admin options page content generator. Basically it is an Vue app holder container and basic app template
 */
if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\gePluginStarterOptionsPageContent')) {
    function gePluginStarterOptionsPageContent()
    {
        global $geConfig;
        $sRouteVarName = $geConfig->get('routeVarName', 'szkoda_koordynator');
        ?>
    <div id="szkoda_koordynator" srcparam="<?php echo $sRouteVarName ?>">
        <main-menu></main-menu>
        <notifications></notifications>
        <loading v-if="loading || manifestLoading"
                 :loading-msg="true"
        ></loading>
        <router v-else :routes="groutes" :srcparam="'<?php echo $sRouteVarName ?>'" ref="router"></router>

        </div><?php
    }
}

/*
 * WP Admin Option page declaration
 * */
if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\OptionsPage')) {
    function OptionsPage()
    {
        global $geConfig, $aOptionsPageParams;
        $sMenuTitle = $geConfig->get('optionsPageMenuTitle', 'szkoda_koordynator');
        $sOptionsPageTitle = $geConfig->get('optionsPageTitle', 'szkoda_koordynator');
        $sOptionsPageSlug = $geConfig->get('optionsPageSlug', 'szkoda_koordynator');
        \add_menu_page($sOptionsPageTitle, $sMenuTitle, 'manage_options', $sOptionsPageSlug, '\Gaad\SzkodaKoordynator\Core\Filters\gePluginStarterOptionsPageContent');
        \add_filter('menu_page_capability_' . $sOptionsPageSlug, function () {
            return 'edit_posts';
        });
    }
}


/*
 * Transport  API access data to frontend application ( Client)
 * */
if (!function_exists('\Gaad\SzkodaKoordynator\Core\Filters\backendDataTransport')) {
    function backendDataTransport()
    {
        global $geConfig;
        $iCurrentUserID = get_current_user_id();
        $oCurrentUser = new \WP_User($iCurrentUserID);

        $sAuthHost = 'http://';
        if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
            $sAuthHost = 'https://';
        }

        $sAuthHost .= $_SERVER['HTTP_HOST'];
        if (!empty($oCurrentUser->roles)) {
            $aApiAccess = [
                'uid' => $iCurrentUserID,
                'domain' => $sAuthHost
            ];
            $aGunnerCapsWhiteList = $geConfig->get("caps", "all-caps");
            $geUserCaps = $oCurrentUser->allcaps;
            $geUserRoles = $oCurrentUser->roles;
            $aFilteredCaps = [];
            if (is_array($aGunnerCapsWhiteList))
                foreach ($aGunnerCapsWhiteList as $enabledCapName) {
                    $isRegExp = $enabledCapName[0] === '/';
                    foreach ($geUserCaps as $capName => $enabled) {
                        //if (!$enabled) continue;
                        if ($isRegExp) {
                            unset($matches);
                            preg_match($enabledCapName, $capName, $matches, PREG_OFFSET_CAPTURE, 0);
                            if ($matches) {
                                $aFilteredCaps[$matches[0][0]] = $enabled;
                            }

                        } else
                            if ($enabledCapName === $capName) {
                                $aFilteredCaps[$enabledCapName] = $enabled;
                            }
                    }
                }
            $geUserRoles = array_values($geUserRoles);
        }
        ?>
        <script id="apiAccess" type="application/javascript">
            window['geUserCaps'] = {
                'caps': <?php echo json_encode($aFilteredCaps, true); ?>,
                'roles': <?php echo json_encode($geUserRoles, true); ?>
            };
            window['geConfig'] = <?php echo $geConfig->getJSON(); ?>;
            window['apiAccess'] = <?php echo json_encode($aApiAccess); ?>;
        </script>
        <?php
    }
}