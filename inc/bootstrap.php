<?php

namespace Gaad\SzkodaKoordynator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Gaad\Gendpoints\Config\Config;
use Gaad\SzkodaKoordynator\Forms\caseDetailsContactForm7Form;
use Gaad\SzkodaKoordynator\Handlers\VueComponents;

new caseDetailsContactForm7Form();

$GLOBALS['aGEConfigDirectories'] = $GLOBALS['aSZKODA_KOORDYNATORConfigDirectories'] = array_values(array_unique(\apply_filters("ge-config-directories", array_merge([
    __SZKODA_KOORDYNATOR_DIR__ . '/config',
], is_array($GLOBALS['aGEConfigDirectories']) ? $GLOBALS['aGEConfigDirectories'] : []))));

$GLOBALS['geConfig'] = new Config('general.yaml', $GLOBALS['aGEConfigDirectories']);

if (!defined('WP_CLI')) {
    \add_filter('ge-config-directories', '\Gaad\SzkodaKoordynator\Core\Filters\configDirectories');
    \add_filter('ge-endpoints-directories', '\Gaad\SzkodaKoordynator\Core\Filters\endpointsDirectories');

    if (is_admin()) {
        //Add wp-admin options page for planner Vue App
        \add_action('admin_menu', '\Gaad\SzkodaKoordynator\Core\Filters\OptionsPage');

        $geConfig = new Config('general.yaml', $GLOBALS['aSZKODA_KOORDYNATORConfigDirectories']);
        $sOptionsPageSlug = $geConfig->get('optionsPageSlug', 'szkoda_koordynator');
        if (isset($_GET['page']) && $sOptionsPageSlug === $_GET['page']) {
            //External scripts loading
            \add_action('admin_enqueue_scripts', '\Gaad\SzkodaKoordynator\Core\Filters\buildAssetsQueue');
            \add_action('admin_enqueue_scripts', '\Gaad\SzkodaKoordynator\Core\Filters\buildStylesQueue');

            \add_action('admin_init', '\Gaad\SzkodaKoordynator\Core\Filters\szkoda_koordynatorOptionsPageRedirect');
            //transport data from backend to frontend
            \add_action('in_admin_header', '\Gaad\SzkodaKoordynator\Core\Filters\backendDataTransport');
            //Vue templates
            \add_action('in_admin_header', '\Gaad\SzkodaKoordynator\Core\Filters\vueTemplatesLoading');
            //enable defer for vue app - waiting until components are loaded
            \add_action('script_loader_tag', '\Gaad\SzkodaKoordynator\Core\Filters\vueAppScriptDefer', 3, 10);
        }
    } else {

        //External scripts loading
        \add_action('wp_enqueue_scripts', '\Gaad\SzkodaKoordynator\Core\Filters\buildAssetsQueue');
        \add_action('wp_enqueue_scripts', '\Gaad\SzkodaKoordynator\Core\Filters\buildStylesQueue');

    }
}

$proxyTmpDir = explode("/wp-content", dirname(__FILE__))[0] . "/.proxy-tmp";
if (!is_dir($proxyTmpDir)) @mkdir($proxyTmpDir);
/*
 * Doctrine Entity Manager
 */
if (!isset($GLOBALS['oGAEntityManager']))
    $GLOBALS['oGAEntityManager'] = EntityManager::create(
        ['driver' => 'pdo_mysql', 'host' => \DB_HOST, 'user' => \DB_USER, 'password' => \DB_PASSWORD, 'dbname' => \DB_NAME],
        Setup::createAnnotationMetadataConfiguration([__SZKODA_KOORDYNATOR_DIR__ . "/config"], ENV === 'dev' ?? false, $proxyTmpDir, null, false)
    );

$GLOBALS['oGAEntityManager']
    ->getConnection()
    ->getDatabasePlatform()
    ->registerDoctrineTypeMapping('enum', 'string');


$GLOBALS['oSZKODA_KOORDYNATORVueComponents'] = new VueComponents([__SZKODA_KOORDYNATOR_DIR__ . '/config']);
\add_filter('init', '\Gaad\SzkodaKoordynator\Core\Filters\userRolesAdd');
\add_filter('init', '\Gaad\SzkodaKoordynator\Core\Filters\defaultOptionsInstaller');
\add_filter('init', '\Gaad\SzkodaKoordynator\Core\Filters\defaultSystemElementsInstaller');
\add_filter('init', '\Gaad\SzkodaKoordynator\Core\Filters\viewControllerInitialize', 1, 0);


add_action( 'template_redirect', '\Gaad\SzkodaKoordynator\Core\Filters\redirect_to_manager_page', 1000, 1 );
