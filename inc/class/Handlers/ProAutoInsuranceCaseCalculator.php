<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\Ganer\Interfaces\InsuranceCaseCalculator;
use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;

class ProAutoInsuranceCaseCalculator implements InsuranceCaseCalculator
{
    private $value;
    private $insuranceCaseCalculation;

    private $calculationData = [];

    /**
     * @param InsuranceCase|null $insuranceCase
     * @param int $value
     * @return array|void
     */
    public function calculate(?InsuranceCase $insuranceCase, int $value)
    {
        $this->setValue($value);
        $calculationData = [];
        $calculationData['client'] = $value / 2 * 0.85;
        $calculationData['margin'] = $value / 2 - $calculationData['client'];
        $this->setCalculationData($calculationData);
        $insuranceCaseCalculation = $this->getInsuranceCaseCalculation();
        if ($insuranceCaseCalculation instanceof InsuranceCaseCalculation && $insuranceCase) $this->applyCalculation($insuranceCaseCalculation);
        return $calculationData;
    }


    public function applyCalculation(InsuranceCaseCalculation $insuranceCaseCalculation)
    {
        $calculationData = $this->getCalculationData();
        $insuranceCaseCalculation->setValueClient($calculationData['client']);
        $insuranceCaseCalculation->setValueMargin($calculationData['margin']);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return array
     */
    public function getCalculationData(): array
    {
        return $this->calculationData;
    }

    /**
     * @param array $calculationData
     */
    public function setCalculationData(array $calculationData): void
    {
        $this->calculationData = $calculationData;
    }

    /**
     * @return InsuranceCaseCalculation|null
     */
    public function getInsuranceCaseCalculation(): ?InsuranceCaseCalculation
    {
        return $this->insuranceCaseCalculation;
    }

    /**
     * @param mixed $insuranceCaseCalculation
     */
    public function setInsuranceCaseCalculation($insuranceCaseCalculation): void
    {
        $this->insuranceCaseCalculation = $insuranceCaseCalculation;
    }

    public function getEquationID(): string
    {
        return __CLASS__;
    }
}