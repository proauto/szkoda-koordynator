<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;
use Gaad\Gendpoints\GEndpoint;
use PHPUnit\Util\Exception;

class AppraiserPageGenerator extends PageGenerator
{

    private $insuranceCase;
    private $sessionID;

    /**
     * AppraiserPageGenerator constructor.
     * @param $accessManager
     */
    public function __construct($accessManager)
    {
        parent::__construct($accessManager);
        $this->sessionID = $_GET['cid'];
        $this->insuranceCase = $this->acquireInsuranceCase();
    }

    public function render()
    {
        /** @var AccessManager $accessMenager */
        $accessManager = $this->getAccessManager();
        if (
            !$accessManager->checkOwnership($this->getInsuranceCase())
            && !$accessManager->userIs('administrator')
        ) {
            $this->renderAccessDenied();
            return;
        }

        if ($this->isCaseStatus('active')) {
            $this->renderCaseForm();
            $this->renderCaseFiles();
            $this->renderCaseActions();
            $this->renderCalculationModal();
            $this->renderCalculationAcceptModal();
            $this->renderCalculationDenyModal();
        } else {
            if ($this->isCaseStatus('sent')) {
                $this->renderCaseDone();
                return;
            }
            $this->renderCaseUnActive();
        }
    }

    private function renderCalculationAcceptModal()
    {
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-case-calculation-accept">
            <script type="javascript/template" class="content-template">
                <h3>Czy na pewno chcesz zaakceptować kalkulację?</h3>
            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Akceptacja kalkulacji</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="case-accept">Akceptuj</button>
                        <button type="button" class="btn btn-secondary edit-calculation" data-dismiss="modal">Popraw
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function renderCalculationDenyModal()
    {
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-case-calculation-deny">
            <script type="javascript/template" class="content-template">
                <h3>Czy na pewno chcesz odrzucić sprawę?</h3>
            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Odrzucenie sprawy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger case-deny" id="case-deny">Odrzuć</button>
                        <button type="button" class="btn btn-info case-calc-show">Zobacz</button>
                        <button type="button" class="btn btn-secondary edit-calculation" data-dismiss="modal">Popraw
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function renderCalculationModal()
    {
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-case-calculation">
            <script type="javascript/template" class="content-template">
                <h1>Kalkulacja</h1>
                Wartość: <%= value %><br>
                Prowizja: <%= margin %><br>
                Wypłata klienta: <%= client %><br>

            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Kalkulacja</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary case-accept">Akceptuj</button>
                        <button type="button" class="btn btn-secondary edit-calculation">Popraw</button>
                        <button type="button" class="btn btn-danger case-deny">Odrzuć</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function renderCaseForm()
    {
        $accessMenager = $this->getAccessManager();
        $userId = $accessMenager->getUser()->getID();

        $this->getAccessManager()->userIs('administrator')
        || ($userId === $this->insuranceCase->getOwner() && ($this->getAccessManager()->userIs('rzeczoznawca')))
        && null !== $this->insuranceCase
        && $this->insuranceCase->isActive()
            ?
            $this->renderCasePage()
            : $this->renderAccessDenied();
    }

    private function prepareCaseDataForContactForm7()
    {
        $_GET['cuid'] = get_current_user_id();
        foreach ($this->insuranceCase->getMeta() as $meta) {
            $_GET[$meta->getMetaName()] = $meta->getMetaValue();
        }
    }

    private function renderCasePage()
    {
        ob_start();
        include $this->getCaseTemplatePath();
        echo ob_get_clean();
    }

    private function getCaseTemplatePath()
    {
        return __SZKODA_KOORDYNATOR_DIR__ . "/inc/Templates/appraiser-case-page.php";
    }

    private function acquireInsuranceCase(): ?InsuranceCase
    {
        global $oGEEntityManager;
        $insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
        $insuranceCaseMetaRepository = $oGEEntityManager->getRepository(InsuranceCaseMeta::class);
        $insuranceCase = $insuranceCaseRepository->findOneBy(["sessionID" => $this->sessionID]);
        if ($insuranceCase instanceof InsuranceCase) {
            $insuranceCaseMeta = $insuranceCaseMetaRepository->findBy(['insuranceCase' => $insuranceCase->getId()]);
            if (!empty($insuranceCaseMeta)) {
                $insuranceCase->setMeta($insuranceCaseMeta);
            }
            return $insuranceCase;
        }
        return null;
    }

    private function renderCaseFiles()
    {
        global $oGEEntityManager;
        ?><h3>Pliki dołączone do sprawy:</h3><?php

        $insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
        $insuranceCase = $insuranceCaseRepository->findOneBy(["sessionID" => $_GET['cid']]);
        if (!($insuranceCase instanceof InsuranceCase)) return false;

        $files = new GEndpoint("/v1/data/szkoda/files-modal", ["aHeaders" => ["Caseid" => $insuranceCase->getID()]]);
        $result = $files->output();
        if ($result) {
            try {
                $result = \json_decode($result, true);
            } catch (Exception $e) {
            }
        }
        if (isset($result["status"]) && "success" === $result["status"]) {
            $filesArr = $result["data"]["Module-Data"]["files"];

            if (!empty($filesArr)) {
                ?>
                <ul><?php
                foreach ($filesArr as $slug => $url) {
                    ?>
                    <li><a target="_blank" href="<?php echo $url; ?>"><?php
                        $array = explode("/", $url);
                        echo array_pop($array)
                        ?></a></li><?php

                }
                ?></ul><?php
            }
        }
        ?>
        <?php
    }

    private function renderCaseActions()
    {
        ?><h3>Możliwe operacje:</h3><?php
        ?>
        <button type="button" class="btn btn-primary" id="calculate-case">Oblicz</button>
        <button type="button" disabled class="btn btn-info case-calc-show">Pokaż szczegóły kalkulacji</button>
        <button type="button" class="btn btn-danger case-deny">Odrzuć sprawę</button>
        <?php
    }


    /**
     * @return string
     */
    private function getCaseStatus(): string
    {
        global $oGEEntityManager;
        $criteria = ['sessionID' => $this->sessionID];
        $InsuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
        $calculation = $InsuranceCaseRepository->findOneBy($criteria);
        if ($calculation instanceof InsuranceCase) {
            return $calculation->getStatus();
        }
        return InsuranceCase::STATUS_BLOCKED;
    }

    /**
     * @param string $status
     * @return bool
     */
    private function isCaseStatus(string $status): bool
    {
        return $this->getCaseStatus() === $status;
    }

    /**
     *
     */
    private function renderCaseDone()
    {
        ?><h1>Sprawa zakończona</h1><?php
    }

    /**
     *
     */
    private function renderCaseUnActive()
    {
        ?><h1>Sprawa już nie jest aktywna</h1><?php
    }

    /**
     * @return InsuranceCase|null
     */
    public function getInsuranceCase(): ?InsuranceCase
    {
        return $this->insuranceCase;
    }


}