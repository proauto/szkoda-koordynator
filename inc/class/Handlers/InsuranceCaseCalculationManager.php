<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Doctrine\ORM\ORMException;
use Gaad\Ganer\Interfaces\InsuranceCaseCalculator;
use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;

class InsuranceCaseCalculationManager
{

    private $hash = null;
    private $value = null;
    private $description = null;
    private $insuranceCase = null;
    private $calculation = null;
    private $calculator = null;
    private $owner = null;

    /**
     * InsuranceCaseCalculationManager constructor.
     * @param InsuranceCase $insuranceCase
     * @param InsuranceCaseCalculator $calculator
     */
    public function __construct(InsuranceCase $insuranceCase, InsuranceCaseCalculator $calculator)
    {
        $this->calculator = $calculator;
        $this->insuranceCase = $insuranceCase;
    }


    public function createCalculation($value)
    {
        $calculation = new InsuranceCaseCalculation();
        $this->setValue($value);
        $calculation->setValue($value);
        $calculator = $this->getCalculator();
        if ($calculator) {
            $calculator->setInsuranceCaseCalculation($calculation);
            $calculator->calculate($this->getInsuranceCase(), $this->getValue());
        }
        $this->setCalculation($calculation);
    }

    /**
     * @return null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param null $value
     */
    public function setValue($value): void
    {
        $this->value = (int)$value;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description): void
    {
        $calculation = $this->getCalculation();
        if ($calculation instanceof InsuranceCaseCalculation) {
            $calculation->setDescription($description);
        }
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getInsuranceCase()
    {
        return $this->insuranceCase;
    }

    /**
     * @param InsuranceCase $insuranceCase
     */
    public function setInsuranceCase(InsuranceCase $insuranceCase): void
    {
        $calculation = $this->getCalculation();
        if ($calculation instanceof InsuranceCaseCalculation) {
            $calculation->setInsuranceCase($insuranceCase);
        }
        $this->insuranceCase = $insuranceCase;
    }

    /**
     * @return null
     */
    public function getCalculation()
    {
        return $this->calculation;
    }

    /**
     * @param null $calculation
     */
    public function setCalculation($calculation): void
    {
        $this->calculation = $calculation;
        $this->calculation->setEquation($this->getCalculator()->getEquationID());
    }

    /**
     * @return InsuranceCaseCalculator
     */
    public function getCalculator(): ?InsuranceCaseCalculator
    {
        return $this->calculator;
    }

    /**
     * @param InsuranceCaseCalculator $calculator
     */
    public function setCalculator(InsuranceCaseCalculator $calculator): void
    {
        $this->calculator = $calculator;
    }

    /**
     * @return null
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param null $owner
     */
    public function setOwner($owner): void
    {
        $calculation = $this->getCalculation();
        if ($calculation instanceof InsuranceCaseCalculation) {
            $calculation->setOwner($owner);
        }
        $this->owner = $owner;
    }

    /**
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveCalculation()
    {
        global $oGEEntityManager;
         try {
        $calculation = $this->getCalculation();
        $oGEEntityManager->persist($calculation);
        $oGEEntityManager->flush();
          } catch (ORMException $e) {
             throw $e;
         }
         return true;
    }

    public function setHash($hash)
    {
        $calculation = $this->getCalculation();
        if ($calculation instanceof InsuranceCaseCalculation) {
            $calculation->setHash($hash);
        }
        $this->hash = $hash;
    }


}