<?php


namespace Gaad\SzkodaKoordynator\Handlers;


class KoordynatorViewController
{
    private $accessManager;

    /**
     * KoordynatorViewController constructor.
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        $this->accessManager = $accessManager;

        \add_action('koordynator_manager_page_content', [$this, 'managerPageContent'], 1000, 2);
        \add_action('koordynator_appraiser_page_content', [$this, 'appraiserPageContent'], 1000, 2);
    }


    public function managerPageContent(\WP_Post $post)
    {
        $page = new ManagerPageGenerator($this->getAccessManager());
        $page->render();
    }

    public function appraiserPageContent(\WP_Post $post)
    {
        $page = new AppraiserPageGenerator($this->getAccessManager());
        $page->render();
    }

    /**
     * @return AccessManager
     */
    public function getAccessManager(): AccessManager
    {
        return $this->accessManager;
    }


}