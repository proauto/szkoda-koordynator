<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Firebase\JWT\JWT;
use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseCalculation;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;
use Gaad\Gendpoints\Entity\User;
use function Gaad\SzkodaKoordynator\Core\Filters\getOptionValue;

class SzkodaRecordManager
{
    const MASTER_APPRAISER_OPTION = 'master_appraiser';
    const APPRAISER_ROLE = 'rzeczoznawca';
    const MANAGER_ROLE = 'koordynator';

    private $masterAppraiser;
    private $accessManager;

    /**
     * rzeczoznawcaManager constructor.
     * @param AccessManager $accessManager
     */
    public function __construct(AccessManager $accessManager)
    {
        $this->accessManager = $accessManager;
    }


    public function setOwner(szkodaRecord $oRecord, User $oUser = NULL)
    {
        global $oGAEntityManager;

        if (is_null($oUser)) {
            $oUser = $this->getMasterAppraiser();
        }
        $oInsuranceCase = $oRecord->getInsuranceCase();
        $oInsuranceCase->setOwner($oUser);
        $oGAEntityManager->persist($oInsuranceCase);
        $oGAEntityManager->flush();
    }

    public function setMasterAppraiser()
    {
        global $oGAEntityManager;
        if (!$this->masterAppraiser) {
            $iMaster = (int)getOptionValue(self::MASTER_APPRAISER_OPTION);
            if (0 === $iMaster) {
                $aAppraisers = $this->getAppraisers();
                $oAppraiser = array_shift($aAppraisers);
                update_option(self::MASTER_APPRAISER_OPTION, $oAppraiser->ID);
                $iMaster = $oAppraiser->ID;
            }
            $oUserRepository = $oGAEntityManager->getRepository(User::class);
            $this->masterAppraiser = $oUserRepository->findOneBy(['ID' => $iMaster]);
        }
    }

    /**
     * @return User
     */
    public function getMasterAppraiser(): User
    {
        if (is_null($this->masterAppraiser))
            $this->setMasterAppraiser();
        return $this->masterAppraiser;
    }

    private function getAppraisers()
    {
        $args = array(
            'role' => self::APPRAISER_ROLE,
            'orderby' => 'ID',
            'order' => 'ASC'
        );
        return get_users($args);
    }

    private function getManagers()
    {
        $args = array(
            'role' => self::MANAGER_ROLE,
            'orderby' => 'ID',
            'order' => 'ASC'
        );
        return get_users($args);
    }

    public function getTheList()
    {
        global $oGAEntityManager;
        $sJWTToken = str_replace("Bearer ", "", getallheaders()['Authorization']);
        $aCasesFormatted = [];
        if ($sJWTToken) {
            global $geConfig;
            $oJWT = JWT::decode($sJWTToken, $geConfig->get('jwtSecretKey'), ['HS256']);

            if ($oJWT instanceof \StdClass) {
                $oCasesRepository = $oGAEntityManager->getRepository(InsuranceCase::class);
                $oCaseMetaRepository = $oGAEntityManager->getRepository(InsuranceCaseMeta::class);

                //dokonanie wyboru, ktoregoownera zwrócić względem praw

                $aCases = $oCasesRepository->findAll();
                foreach ($aCases as $oCase) {
                    $oCaseMeta = $oCaseMetaRepository->findBy(['insuranceCase' => $oCase->getId()]);
                    $oCase->setMeta($oCaseMeta);
                    $aCasesFormatted[] = $oCase->toArray();
                }
            }
        }
        return $aCasesFormatted;
    }

    public function getManager()
    {
        global $oGEEntityManager;
        $managers = $this->getManagers();
        $userRepository = $oGEEntityManager->getRepository(User::class);
        return $userRepository->findOneBy(["ID" => $managers[0]->ID]);
    }

}