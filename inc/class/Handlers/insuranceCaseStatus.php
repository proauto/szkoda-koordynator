<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\Gendpoints\Entity\InsuranceCase;

class insuranceCaseStatus
{

    public function getAllStatuses()
    {
        return [
            InsuranceCase::STATUS_SENT,
            InsuranceCase::STATUS_BLOCKED,
            InsuranceCase::STATUS_ACTIVE,
            InsuranceCase::STATUS_DENIED,
        ];
    }

}