<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\SzkodaKoordynator\Forms\caseDetailsEditAdminContactForm7Form;
use Gaad\SzkodaKoordynator\Forms\caseDetailsEditManagerContactForm7Form;

class ManagerPageGenerator extends PageGenerator
{


    /**
     * ManagerPageGenerator constructor.
     * @param $accessManager
     */
    public function __construct($accessManager)
    {
        parent::__construct($accessManager);
    }

    public function render()
    {
        $this->getAccessManager()->userIs('administrator')
        || $this->getAccessManager()->userIs('koordynator') ?
            $this->renderManager()
            : $this->renderAccessDenied();
    }

    private function renderTable()
    {
        ?>

        <table id="example" class="table table-bordered table-responsive-sm table-responsive-md" style="width:100%">

            <thead>
            <tr>
                <th rowspan="2">ID</th>
                <th colspan="13">Dane sprawy</th>

                <th rowspan="2">Akcje</th>

            </tr>
            <tr>
                <th>Status</th>
                <th>Rzeczoznawca</th>
                <th>Utworzona</th>
                <th>Zmodyfikowana</th>
                <th>Imię i nazwisko</th>
                <th>Nr. telefonu</th>
                <th>E-mail</th>
                <th>Szkoda z</th>
                <th>Szkoda w PL</th>
                <th>Kalkulacja Utworzona</th>
                <th>Różnica</th>
                <th>Prowizja</th>
                <th>Klient</th>
            </tr>


            </thead>
            <tbody>

            </tbody>
        </table>
        <?php
    }

    private function renderManager()
    {
        $this->renderHeader();
        $this->renderTable();
        $this->renderFilesModal();
        $this->appraiserLinkResendModal();
        $this->renderEditModal();
        $this->renderRemoveModal();
    }

    private function appraiserLinkResendModal()
    {
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-appraiser-send-link">
            <script type="javascript/template" class="content-template">
                <% if(appraiserUrl) { %>
                Link do sprawy: <a target="_blank" href="<%= appraiserUrl %>"><%= appraiserUrl %></a>
                <% } %>
            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Wiadomość została wysłana do rzeczoznawcy</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function renderFilesModal()
    {
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-case-files">
            <script type="javascript/template" class="content-template">
                <% if(files) { %>
                <ol>
                    <% for(var i in files) { %>
                    <li><a target="_blank" href="<%= files[i] %>"><%= files[i].split("/").reverse()[0] %></a></li>
                    <% } %>
                </ol>
                <% } %>
            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Pobierz pliki rekordu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Zgłaszający dołączył do sprawy następujące pliki:</p>
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function renderEditModal()
    {
        $userID = $this->getAccessManager()->getUser()->getId();
        $user = new \WP_User($userID);
        if ($user->has_cap('administrator')) {
            $caseDatailsEditForm = new caseDetailsEditAdminContactForm7Form();

        } elseif ($user->has_cap('koordynator')) {
            $caseDatailsEditForm = new caseDetailsEditManagerContactForm7Form();

        }
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-edit-case">
            <script type="javascript/template" class="content-template">

            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edytuj rekord szkody</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="loading-overlay"><i class="fas fa-spinner fa-spin"></i></div>
                    <div class="modal-body">
                        <?php
                        echo do_shortcode($caseDatailsEditForm->getShortcode());
                        ?>
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-recalculate" disabled id="case-recalculate">
                            Przelicz
                        </button>
                        <button type="button" class="btn btn-primary" id="case-edit-execute">Edytuj</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function renderRemoveModal()
    {
        $userID = $this->getAccessManager()->getUser()->getId();
        $user = new \WP_User($userID);
        if ($user->has_cap('administrator')) {
            $caseDatailsEditForm = new caseDetailsEditAdminContactForm7Form();

        } elseif ($user->has_cap('koordynator')) {
            $caseDatailsEditForm = new caseDetailsEditManagerContactForm7Form();

        }
        ?>
        <div class="modal" tabindex="-1" role="dialog" id="modal-remove-case">
            <script type="javascript/template" class="content-template">
            </script>
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Usuń rekord szkody</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="loading-overlay"><i class="fas fa-spinner fa-spin"></i></div>
                    <div class="modal-body">
                        <p>Usunąć sprawę?</p>
                        <div class="content-inject"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="case-remove-execute">Usuń</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function renderHeader()
    {
        ?><h1>Lista zgłoszonych szkód</h1><?php
    }
}