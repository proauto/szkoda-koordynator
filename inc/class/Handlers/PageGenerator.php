<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\Ganer\Interfaces\PageGeneratorInterface;
use Gaad\Gendpoints\Entity\User;
use Gaad\SzkodaKoordynator\Forms\signInContactForm7Form;

class PageGenerator implements PageGeneratorInterface
{
    private $accessManager;

    /**
     * PageGenerator constructor.
     * @param $accessManager
     */
    public function __construct($accessManager)
    {
        $this->accessManager = $accessManager;
    }

    public static function loginForm()
    {
        ?>
        <div class="loginFormWrapper">
            <?php
            $signInContactForm = new signInContactForm7Form();
            echo do_shortcode($signInContactForm->getShortcode()); ?>
        </div>
        <?php
    }

    public function render()
    {
        echo "";
    }

    /**
     * @return mixed
     */
    public function getAccessManager()
    {
        return $this->accessManager;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    public static function renderAccessDenied()
    {
        echo "Error: Access Denied!!!";
    }

}