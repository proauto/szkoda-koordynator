<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\InsuranceCaseMeta;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPUnit\Util\Exception;
use ZipArchive;

class szkodaRecord
{
    const LOG_PATH = "/tmp/koordynator-api.log";
    const ATTACHMENTS_NAMES = ["insurance-carrier-calculation", "file-custom-1", "file-custom-2", "file-custom-3", "file-custom-4", "file-custom-5"];

    private $sessionID;
    private $zipFile;
    private $tempFolder;
    private $recordFolder;
    private $uploadsFolder;
    /**
     * @var InsuranceCase
     */
    private $insuranceCase;
    /**
     * @var Logger
     */
    private $log;

    /**
     * szkodaRecord constructor.
     * @param $sessionID
     * @param $zipFile
     */
    public function __construct($sessionID, $zipFile)
    {
        $this->sessionID = $sessionID;
        $this->zipFile = $zipFile;
        $this->uploadsFolder = wp_get_upload_dir();

        $this->log = new Logger(__CLASS__);
        $this->log->pushHandler(new StreamHandler(self::LOG_PATH, Logger::INFO));
    }

    public function unzipData()
    {
        if ($this->validateZipFile() && $this->createRecordFolder()) {
            $zip = new \ZipArchive();
            copy($this->getZipFile()['tmp_name'], $this->getRecordFolder() . "/zip");
            if ($zip->open($this->getRecordFolder() . "/zip")) {
                $zip->extractTo($this->getRecordFolder());
                unlink($this->getRecordFolder() . "/zip");
            }
            $zip->close();
            return true;
        }
        return false;
    }

    public function processRecord()
    {
        if ($this->unzipData()) {
            $this->createRecordEntity();
            $this->applyRecordMeta();
        }
    }

    private function validateZipFile()
    {
        return true;
    }


    private function createTempFolder()
    {
        if ("" === $this->tempFolder) {
            $this->tempFolder = sys_get_temp_dir() . "/" . $this->getSessionID();
            @mkdir($this->tempFolder);
        }
        $this->bundleZipFile = $this->getTempFolder() . '/data.zip';
    }

    /**
     * @return mixed
     */
    public function getSessionID()
    {
        return $this->sessionID;
    }

    /**
     * @return mixed
     */
    public function getZipFile()
    {
        return $this->zipFile;
    }

    /**
     * @return mixed
     */
    public function getTempFolder()
    {
        return $this->tempFolder;
    }

    private function createRecordFolder()
    {
        $sRecordDir = $this->getUploadsFolder() . "/" . $this->getSessionID();
        if (!is_dir($sRecordDir)) @mkdir($sRecordDir);
        $this->recordFolder = $sRecordDir;
        return is_dir($sRecordDir);
    }

    /**
     * @return mixed
     */
    public function getRecordFolder()
    {
        return $this->recordFolder;
    }

    /**
     * @return mixed
     */
    public function getUploadsFolder()
    {
        return $this->uploadsFolder['basedir'];
    }

    private function createRecordEntity()
    {
        global $oGAEntityManager;
        $this->insuranceCase = new InsuranceCase();
        $this->insuranceCase->setSessionID($this->getSessionID());
        $oGAEntityManager->persist($this->insuranceCase);
        $oGAEntityManager->flush();
    }

    /*
     * Persists record meta data into db
     */
    private function applyRecordMeta()
    {
        global $oGAEntityManager;
        $aMeta = $this->getMetaFromDataFile();

        if ($aMeta) {
            foreach ($this->mergeAttachmetsMeta($aMeta) as $sMetaName => $sMetaValue) {
                $oMeta = new InsuranceCaseMeta();
                $oMeta->setInsuranceCase($this->insuranceCase);
                $oMeta->setMetaName($sMetaName);
                $oMeta->setMetaValue(is_array($sMetaValue) ? serialize($sMetaValue) : $sMetaValue);
                $oGAEntityManager->persist($oMeta);
            }
            $oGAEntityManager->flush();
            //$this->log->error("Koordynator API PUT success, sessionID={$this->getSessionID()}");
        }
    }

    private function getMetaFromDataFile()
    {
        $sRecordMeta = file_get_contents($this->recordFolder . "/mail-tags.json");
        $aMeta = null;

        try {
            $aMeta = \json_decode($sRecordMeta, true);
        } catch (Exception $e) {

            //$this->log->error("Koordynator API PUT error, sessionID={$this->getSessionID()}");
        }

        return $aMeta;
    }

    private function mergeAttachmetsMeta($aMeta)
    {
        $aMeta["sessionID"] = $this->getSessionID();
        foreach (self::ATTACHMENTS_NAMES as $sName) {
            if (array_key_exists($sName, $aMeta)) {
                $sAttachmentPath = $this->getRecordFolder() . "/" . $aMeta[$sName];
                if (is_file($sAttachmentPath)) {
                    $aMeta[$sName] = $sAttachmentPath;
                }
            }
        }
        return $aMeta;
    }

    /**
     * @return InsuranceCase
     */
    public function getInsuranceCase(): InsuranceCase
    {
        return $this->insuranceCase;
    }


}