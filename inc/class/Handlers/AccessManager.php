<?php


namespace Gaad\SzkodaKoordynator\Handlers;


use Gaad\Gendpoints\Entity\User;

class AccessManager
{
    /**
     * @var User
     */
    private $user;

    /**
     * AccessManager constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function checkOwnership($entity)
    {
        $owner = $entity->getOwner();
        if (!$owner || is_null($owner)) return false;
        return $this->getUser()->getID() === $owner;
    }

    public function userIs($roleName)
    {
        $wpUser = new \WP_User($this->getUser()->getID());
        return in_array($roleName, $wpUser->roles);
    }

    public function userCan($capName)
    {
        $wpUser = new \WP_User($this->getUser()->getID());
        return $this->userIs("administrator") || (isset($wpUser->get_role_caps()[$capName]) && $wpUser->get_role_caps()[$capName]);
    }

    public function getUser()
    {
        return $this->user;
    }


}