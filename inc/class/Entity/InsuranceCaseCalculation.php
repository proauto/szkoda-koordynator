<?php

namespace Gaad\Gendpoints\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Gaad\Gendpoints\Entity\Options
 *
 * @ORM\Entity
 * @ORM\Table(name="pa_insurance_case_calculation")
 **/
class InsuranceCaseCalculation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ID;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="ID")
     */
    private $owner;

    /**
     * @ManyToOne(targetEntity="InsuranceCase")
     * @JoinColumn(name="insurance_case_id", referencedColumnName="ID")
     */
    private $insuranceCase;

    /** @ORM\Column(type="datetime") * */
    private $created_at;

    /** @ORM\Column(type="text", nullable=true) * */
    private $description;

    /** @ORM\Column(type="integer") * */
    private $value;

    /** @ORM\Column(type="integer") * */
    private $valueClient;

    /** @ORM\Column(type="integer") * */
    private $valueMargin;

    /** @ORM\Column(type="text", nullable=true) * */
    private $equation;

    /** @ORM\Column(type="text", nullable=true) * */
    private $hash;

    /**
     * InsuranceCaseCalculation constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt();
    }


    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID): void
    {
        $this->ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     */
    public function getInsuranceCase()
    {
        return $this->insuranceCase;
    }

    /**
     * @param mixed $insuranceCase
     */
    public function setInsuranceCase($insuranceCase): void
    {
        $this->insuranceCase = $insuranceCase;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     *
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue(float $value): void
    {
        $this->value = (float)$value;
    }

    /**
     * @return mixed
     */
    public function getValueClient()
    {
        return $this->valueClient;
    }

    /**
     * @param mixed $valueClient
     */
    public function setValueClient($valueClient): void
    {
        $this->valueClient = $valueClient;
    }

    /**
     * @return mixed
     */
    public function getValueMargin()
    {
        return $this->valueMargin;
    }

    /**
     * @param mixed $valueMargin
     */
    public function setValueMargin($valueMargin): void
    {
        $this->valueMargin = $valueMargin;
    }

    /**
     * @return mixed
     */
    public function getEquation()
    {
        return $this->equation;
    }

    /**
     * @param mixed $equation
     */
    public function setEquation($equation): void
    {
        $this->equation = $equation;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return  array
     */
    public function toArray(): array
    {
        $return = [
            'id' => $this->getID(),
            'created' => $this->getCreatedAt(),
            'value' => (float)number_format($this->getValue(), 2, ".",""),
            'client' => (float)number_format($this->getValueClient(), 2, ".",""),
            'margin' => (float)number_format($this->getValueMargin(), 2, ".","")
        ];

       /* $return = [
            'id' => $this->getID(),
            'created' => $this->getCreatedAt(),
            'value' => (float)$this->getValue(),
            'client' => (float)$this->getValueClient(),
            'margin' => (float)$this->getValueMargin()
        ];*/
        return $return;

    }


}