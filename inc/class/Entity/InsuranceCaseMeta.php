<?php

namespace Gaad\Gendpoints\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Gaad\Gendpoints\Entity\Options
 *
 * @ORM\Entity
 * @ORM\Table(name="pa_insurance_case_meta")
 **/
class InsuranceCaseMeta
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ID;

    /**
     * @ManyToOne(targetEntity="InsuranceCase")
     * @JoinColumn(name="insurance_case_id", referencedColumnName="ID")
     */
    private $insuranceCase;

    /** @ORM\Column(type="string", length=255) * */
    private $meta_name;

    /** @ORM\Column(type="text") * */
    private $meta_value;

    /** @ORM\Column(type="datetime") * */
    private $created_at;

    /**
     * InsuranceCaseMeta constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt();
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @return mixed
     */
    public function getInsuranceCase()
    {
        return $this->insuranceCase;
    }

    /**
     * @param mixed $insuranceCase
     */
    public function setInsuranceCase($insuranceCase): void
    {
        $this->insuranceCase = $insuranceCase;
    }

    /**
     * @return mixed
     */
    public function getMetaName()
    {
        return $this->meta_name;
    }

    /**
     * @param mixed $meta_name
     */
    public function setMetaName($meta_name): void
    {
        $this->meta_name = $meta_name;
    }

    /**
     * @return mixed
     */
    public function getMetaValue()
    {
        return $this->meta_value;
    }

    /**
     * @param mixed $meta_value
     */
    public function setMetaValue($meta_value): void
    {
        $this->meta_value = $meta_value;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }




}