<?php

namespace Gaad\Gendpoints\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Gaad\Gendpoints\Entity\Options
 *
 * @ORM\Entity
 * @ORM\Table(name="pa_insurance_case")
 **/
class InsuranceCase
{
    const STATUS_ACTIVE = 'active';
    const STATUS_DENIED = 'denied';
    const STATUS_SENT = 'sent';
    const STATUS_BLOCKED = 'blocked';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $ID;

    /**
     * @ManyToOne(targetEntity="InsuranceCaseCalculation")
     * @JoinColumn(name="calculation_id", referencedColumnName="ID", nullable=true)
     */
    private $calculation;

    /** @ORM\Column(type="integer", nullable=true) * */
    private $owner_id;

    /** @ORM\Column(type="string", length=32) * */
    private $sessionID;

    /** @ORM\Column(type="datetime") * */
    private $created_at;

    /** @ORM\Column(type="datetime", nullable=true) * */
    private $modified_at;

    /** @ORM\Column(type="text") */
    private $status = 'active';

    private $meta;

    /**
     * InsuranceCase constructor.
     */
    public function __construct()
    {
        $this->setCreatedAt();
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner_id;
    }

    /**
     * @return mixed
     */
    public function getOwnerO()
    {
        if ($this->owner_id) {
            global $oGEEntityManager;
            $userRepository = $oGEEntityManager->getRepository(User::class);
            $user = $userRepository->findOneBy(["ID" => $this->getOwner()]);
            if ($user instanceof User) {
                return $user;
            }
        }
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner_id = $owner instanceof User ? $owner->getID() : $owner;
    }

    /**
     * @return mixed
     */
    public function getSessionID()
    {
        return $this->sessionID;
    }

    /**
     * @param mixed $sessionID
     */
    public function setSessionID($sessionID): void
    {
        $this->sessionID = $sessionID;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    private function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @return mixed
     */
    public function getMetaValue($key)
    {
        $meta = $this->getMetaArray();
        return array_key_exists($key, $meta) ?  $meta[$key] : null;
    }
        /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @return mixed
     */
    public function getMetaArray()
    {
        $tmp = [];
        foreach ($this->meta as $meta) {
            $tmp[$meta->getMetaName()] = $meta->getMetaValue();
        }
        return $tmp;
    }

    /**
     * @param mixed $meta
     */
    public function setMeta($meta): void
    {
        $this->meta = $meta;
    }


    public function isActive(): bool
    {
        return $this::STATUS_ACTIVE === $this->getStatus();
    }

    public function toArray(): array
    {

        /** @var InsuranceCaseCalculation $calculation */
        $calculation = $this->getCalculation();
        /** @var User $this */
        $owner = $this->getOwnerO();

        $aReturn = [
            "id" => $this->getID(),
            "status" => $this->getStatus(),
            "created" => $this->getCreatedAt(),
            "modified" => !is_null($this->getModifiedAt()) ? $this->getModifiedAt() : ["date" => ""],
        ];
        if ($calculation) {
            $aReturn['calculation'] = $calculation->toArray();
        } else {
            $aReturn['calculation'] = [
                'id' => "",
                'created' => ["date" => ""],
                'value' => "",
                'client' => "",
                'margin' => "",
            ];
        }

        if ($owner) {
            $aReturn['owner'] = $owner->toArray();
        } else {
            $aReturn['owner'] = [
                "ID" => "",
                "displayName" => "",
            ];
        }

        foreach ($this->getMeta() as $oMeta)
            $aReturn[$oMeta->getMetaName()] = $oMeta->getMetaValue();

        return $aReturn;
    }

    public function getAppraiserUrl()
    {

    }


    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCalculation()
    {
        return $this->calculation;
    }

    /**
     * @param mixed $calculation
     */
    public function setCalculation($calculation): void
    {
        $this->calculation = $calculation;
    }

    /**
     * @return mixed
     */
    public function getModifiedAt()
    {
        return $this->modified_at;
    }

    /**
     *
     */
    public function setModifiedAt(): void
    {
        $this->modified_at = new \DateTime();
    }


}