<?php


namespace Gaad\SzkodaKoordynator\Forms;


use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\User;
use Gaad\PaSzkodaWidget\Handlers\KoordynatorApiHandler;
use Gaad\SzkodaKoordynator\Handlers\InsuranceCaseCalculationManager;
use Gaad\SzkodaKoordynator\Handlers\ProAutoInsuranceCaseCalculator;
use WPCF7_ContactForm;
use WPCF7_Submission;

class caseDetailsContactForm7Form
{

    const SLUG = "szczegoly-sprawy";
    const TITLE = "Szczegóły sprawy";

    /**
     * SzkodaContactForm7FormInstaller constructor.
     */
    public function __construct()
    {
        if (class_exists('WPCF7_ContactForm')) {
            \add_action("init", [$this, "checkForm"]);
            \add_action("wpcf7_before_send_mail", [$this, "interceptFormData"], 100, 2);

        }
    }

    public function getShortcode()
    {
        return '[contact-form-7 id="' . $this->getFormID() . '" title="' . $this::TITLE . '"]';
    }

    /**
     * Creates a calculation record
     *
     * @param $oContactForm
     * @param $result
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function interceptFormData($oContactForm, $result)
    {
        global $oGEEntityManager;
        $mail_tags = array_flip($oContactForm->collect_mail_tags());

        foreach ($mail_tags as $item => $value) {
            if (isset($_POST[$item])) {
                $mail_tags[$item] = $_POST[$item];
            }
        }
        $success = false;
        $oSubmission = WPCF7_Submission::get_instance();
        $postedData = $oSubmission->get_posted_data();
        $value = $postedData['appraiser-proposed-value'];
        $description = $postedData['appraiser-case-description'];
        $sessionID = $postedData['cid'];
        $cuID = $postedData['cuid'];
        //tworzenie kalkulacji

        $userRepository = $oGEEntityManager->getRepository(User::class);
        $owner = $userRepository->findOneBy(["ID" => $cuID]);

        $insuranceCaseRepository = $oGEEntityManager->getRepository(InsuranceCase::class);
        $insuranceCase = $insuranceCaseRepository->findOneBy(["sessionID" => $sessionID]);
        if ($insuranceCase instanceof InsuranceCase) {
            $calculationManager = new InsuranceCaseCalculationManager($insuranceCase, new ProAutoInsuranceCaseCalculator());
            $calculationManager->createCalculation($value);
            $calculationManager->setDescription($description);
            $calculationManager->setInsuranceCase($insuranceCase);
            $calculationManager->setHash($oSubmission->get_posted_data_hash());
            if($owner instanceof User) $calculationManager->setOwner($owner);
            $success = $calculationManager->saveCalculation();
        }
/*
        $log = new Logger('szkoda-input');
        $log->pushHandler(new StreamHandler(KoordynatorApiHandler::LOG_PATH, Logger::INFO));

        !$success ?
            $log->error("Koordynator API PUT error, sessionID={$koordynatorApi->getSessionID()}")
            : $log->info("Koordynator API PUT success, sessionID={$koordynatorApi->getSessionID()}");*/
    }

    public function checkForm()
    {
        if (!$this->formExists()) {
            $this->createForm();
        }
    }

    public function getFormID()
    {
        global $wpdb;
        $query = "SELECT `id` FROM {$wpdb->prefix}posts WHERE `post_type` LIKE 'wpcf7_contact_form' AND `post_status` = 'publish' AND `post_name` = '" . $this::SLUG . "';";
        $result = $wpdb->get_results($query, ARRAY_A);
        $formID = (int)array_shift($result)['id'];
        return $formID > 0 ? $formID : null;
    }

    private function formExists()
    {
        return null !== $this->getFormID();
    }

    private function createForm()
    {
        $contact_form = WPCF7_ContactForm::get_template();
        $contact_form->set_title($this::TITLE);

        $props = $contact_form->get_properties();
        $props['additional_settings'] = "demo_mode: on";
        $props['form'] = $this->getFormTemplate();
        $contact_form->set_properties($props);

        //here more form setup
        $contact_form->save();
    }

    private function get_localeMeta()
    {
        return "pl_PL";
    }

    private function get_additional_settingsMeta()
    {
        return NULL;
    }

    private function get_messagesMeta()
    {
        return [
            'mail_sent_ok' => 'Kalkulowanie, proszę czekać...',
            'mail_sent_ng' => 'There was an error trying to send your message. Please try again later.',
            'validation_error' => 'Nie wszystkie pola zostały wypełnione poprawnie.',
            'spam' => 'There was an error trying to send your message. Please try again later.',
            'accept_terms' => 'You must accept the terms and conditions before sending your message.',
            'invalid_required' => 'The field is required.',
            'invalid_too_long' => 'The field is too long.',
            'invalid_too_short' => 'The field is too short.',
        ];
    }

    private function getFormTemplate()
    {
        return <<<EOT

[text cid default:get readonly class:hidden-input]
[text cuid default:get readonly class:hidden-input]
<label>Imię i Nazwisko<span>*</span>
    [text client-full-name default:get readonly class:fake-input]</label>
<label>Numer telefonu<span>*</span>
    [text client-phone-number default:get readonly class:fake-input]</label>
<label>Adres e-mail<span>*</span>
    [text client-email default:get readonly class:fake-input]</label>
<label>Odszkodowanie z<span>*</span>
    [text insurance-type include_blank default:get readonly class:fake-input]</label>
<label>szkoda w Polsce?<span>*</span>
    [text collision-in-pl default:get readonly class:fake-input]</label>
    
<label>Uwagi Rzeczoznawcy<span>*</span>
    [textarea appraiser-case-description default:get]</label>    

<label>Wartość odszkodowania<span>*</span>
    [number* appraiser-proposed-value min:0]</label>    
EOT;

    }

}