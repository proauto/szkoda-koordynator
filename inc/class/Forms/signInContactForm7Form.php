<?php


namespace Gaad\SzkodaKoordynator\Forms;


use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\User;
use Gaad\Gendpoints\GEndpoint;
use Gaad\PaSzkodaWidget\Handlers\KoordynatorApiHandler;
use Gaad\SzkodaKoordynator\Handlers\InsuranceCaseCalculationManager;
use Gaad\SzkodaKoordynator\Handlers\ProAutoInsuranceCaseCalculator;
use WPCF7_ContactForm;
use WPCF7_Submission;

class signInContactForm7Form
{

    const SLUG = "logowanie-do-systemu";
    const TITLE = "Logowanie do systemu";

    /**
     * SzkodaContactForm7FormInstaller constructor.
     */
    public function __construct()
    {
        if (class_exists('WPCF7_ContactForm')) {
            \add_action("init", [$this, "checkForm"]);
            \add_action("wpcf7_before_send_mail", [$this, "interceptFormData"], 100, 2);
            add_filter('wpcf7_feedback_response', [$this, 'wpcf7_feedback_response'], 100, 2);

        }
    }

    /*

    function wpcf7_k_password_validation_filter( $result, $tag ) {
        $name = $tag->name;

        $value = isset( $_POST[$name] )
            ? trim( wp_unslash( strtr( (string) $_POST[$name], "\n", " " ) ) )
            : '';

        if ( 'password' == $tag->basetype ) {
            if ( $tag->is_required() && '' == $value ) {
                $result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
            }
        }

        return $result;
    }
     * */
    public function wpcf7_feedback_response($result, $tag)
    {
        $submission = WPCF7_Submission::get_instance();
        $contactForm = $submission->get_contact_form();
        if ('logowanie-do-systemu' !== $contactForm->name()) return $result;

        $creds = [
            'user_login' => $_POST['l'],
            'user_password' => $_POST['p'],
            'remember' => (bool)$_POST['c']
        ];
        $user = wp_signon($creds, FALSE);
        if (is_wp_error($user)) {
            $result['status'] = "validation_failed";
            $result['message'] = "Podane dane są nieprawidłowe.";
        } else {
            $result['message'] = "Logowanie powiodło się";
        }

        return $result;
    }

    public function getShortcode()
    {
        return '[contact-form-7 id="' . $this->getFormID() . '" title="' . $this::TITLE . '"]';
    }

    /**
     * Creates a calculation record
     *
     * @param $oContactForm
     * @param $result
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function interceptFormData($oContactForm, $result)
    {
        global $oGEEntityManager;
        $mail_tags = array_flip($oContactForm->collect_mail_tags());

        foreach ($mail_tags as $item => $value) {
            if (isset($_POST[$item])) {
                $mail_tags[$item] = $_POST[$item];
            }
        }
        $oSubmission = WPCF7_Submission::get_instance();
        $postedData = $oSubmission->get_posted_data();
        $username = $postedData['l'];
        $password = $postedData['p'];
        $remember = $postedData['r'];
        $creds = [
            'user_login' => $username,
            'user_password' => $password,
            'remember' => (bool)$remember
        ];

        $user = wp_signon($creds, FALSE);
        if (is_wp_error($user)) {
            $aOutput = [
                'success' => FALSE
            ];
            foreach ($user->errors as $errorName => $data) {
                $aOutput['error'] = $errorName;
                break;
            }
        } else {
            wp_set_auth_cookie($user->ID, $creds['remember']);
            $aOutput = [
                'ua' => $user->ID,
                'success' => $user->allcaps['edit_products']
            ];
        }

    }

    public function checkForm()
    {
        if (!$this->formExists()) {
            $this->createForm();
        }
    }

    public function getFormID()
    {
        global $wpdb;
        $query = "SELECT `id` FROM {$wpdb->prefix}posts WHERE `post_type` LIKE 'wpcf7_contact_form' AND `post_status` = 'publish' AND `post_name` = '" . $this::SLUG . "';";
        $result = $wpdb->get_results($query, ARRAY_A);
        $formID = (int)array_shift($result)['id'];
        return $formID > 0 ? $formID : null;
    }

    private function formExists()
    {
        return null !== $this->getFormID();
    }

    private function createForm()
    {
        $contact_form = WPCF7_ContactForm::get_template();
        $contact_form->set_title($this::TITLE);

        $props = $contact_form->get_properties();
        $props['additional_settings'] = "demo_mode: on";
        $props['form'] = $this->getFormTemplate();
        $contact_form->set_properties($props);

        //here more form setup
        $contact_form->save();
    }

    private function get_localeMeta()
    {
        return "pl_PL";
    }

    private function get_additional_settingsMeta()
    {
        return NULL;
    }

    private function get_messagesMeta()
    {
        return [
            'mail_sent_ok' => 'Kalkulowanie, proszę czekać...',
            'mail_sent_ng' => 'There was an error trying to send your message. Please try again later.',
            'validation_error' => 'Nie wszystkie pola zostały wypełnione poprawnie.',
            'spam' => 'There was an error trying to send your message. Please try again later.',
            'accept_terms' => 'You must accept the terms and conditions before sending your message.',
            'invalid_required' => 'The field is required.',
            'invalid_too_long' => 'The field is too long.',
            'invalid_too_short' => 'The field is too short.',
        ];
    }

    private function getFormTemplate()
    {
        return <<<EOT

<label>Login<span>*</span>
    [text* l]</label>
<label>Hasło<span>*</span>
    [password* p]</label>
<label>Zapamiętaj mnie<span>*</span>
    [checkbox r]</label>
    
[submit "Zaloguj"]    
EOT;

    }

}