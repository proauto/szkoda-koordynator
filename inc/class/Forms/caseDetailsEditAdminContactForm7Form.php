<?php


namespace Gaad\SzkodaKoordynator\Forms;


use Gaad\Gendpoints\Entity\InsuranceCase;
use Gaad\Gendpoints\Entity\Post;
use Gaad\Gendpoints\Entity\User;
use Gaad\PaSzkodaWidget\Handlers\KoordynatorApiHandler;
use Gaad\SzkodaKoordynator\Handlers\AccessManager;
use Gaad\SzkodaKoordynator\Handlers\InsuranceCaseCalculationManager;
use Gaad\SzkodaKoordynator\Handlers\insuranceCaseStatus;
use Gaad\SzkodaKoordynator\Handlers\KoordynatorViewController;
use Gaad\SzkodaKoordynator\Handlers\ProAutoInsuranceCaseCalculator;
use WP_User;
use WPCF7_ContactForm;
use WPCF7_Pipes;
use WPCF7_Submission;

class caseDetailsEditAdminContactForm7Form
{

    const SLUG = "szczegoly-sprawy-edycja-admin";
    const TITLE = "Szczegóły sprawy edycja admin";

    /**
     * SzkodaContactForm7FormInstaller constructor.
     */
    public function __construct()
    {
        if (class_exists('WPCF7_ContactForm')) {
            \add_action("init", [$this, "checkForm"]);
            \add_action("wpcf7_before_send_mail", [$this, "interceptFormData"], 100, 2);
            \add_filter('wpcf7_form_tag', [$this, 'dynamic_appraisers_select'], 10, 2);
            \add_filter('wpcf7_form_tag', [$this, 'dynamic_status_select'], 10, 2);

        }
    }

    function dynamic_status_select($scanned_tag, $replace)
    {
        if ($scanned_tag['name'] !== 'case-status') {
            return $scanned_tag;
        }

        $scanned_tag['labels']=[];
        $scanned_tag['raw_values']=[];

        $statusManager = new insuranceCaseStatus();
        $statusList = $statusManager->getAllStatuses();

        if (!empty($statusList)) {
            foreach ($statusList as $status) {
                $scanned_tag['labels'][] = \_x($status, 'case-status', 'szkoda_koordynator');
                $scanned_tag['raw_values'][] = $status;
            }
        }

        global $oGEEntityManager;
        $postRepository = $oGEEntityManager->getRepository(Post::class);
        $customStatus = $postRepository->findBy([
            "post_type" => 'status',
            "post_status" => 'publish'
        ]);

        if (!empty($customStatus)) {
            foreach ($customStatus as $status) {
                $scanned_tag['labels'][] = $status->getPostTitle();
                $scanned_tag['raw_values'][] = $status->getPostTitle();
            }
        }

        $scanned_tag['labels'] = array_unique($scanned_tag['labels']);
        $scanned_tag['raw_values'] = array_unique($scanned_tag['raw_values']);

        $pipes = new WPCF7_Pipes($scanned_tag['raw_values']);

        $scanned_tag['values'] = $pipes->collect_befores();
        $scanned_tag['pipes'] = $pipes;

        $GLOBALS[__FUNCTION__] = true;
        return $scanned_tag;
    }


    function dynamic_appraisers_select($scanned_tag, $replace)
    {
        if ($scanned_tag['name'] !== 'case-owner') {
            return $scanned_tag;
        }
        $scanned_tag['labels'] = [];
        $scanned_tag['raw_values'] = [];

        $args = array(
            'role' => 'rzeczoznawca',
            'orderby' => 'user_nicename',
            'order' => 'ASC'
        );
        $users = get_users($args);
        foreach ($users as $user) {
            $scanned_tag['labels'][] = $user->get('display_name');
            $scanned_tag['raw_values'][] = $user->ID;
        }
        $pipes = new WPCF7_Pipes($scanned_tag['raw_values']);

        $scanned_tag['values'] = $pipes->collect_befores();
        $scanned_tag['pipes'] = $pipes;

        $GLOBALS[__FUNCTION__] = true;
        return $scanned_tag;
    }


    public function getShortcode()
    {
        return '[contact-form-7 id="' . $this->getFormID() . '" title="' . $this::TITLE . '"]';
    }

    /**
     * Creates a calculation record
     *
     * @param $oContactForm
     * @param $result
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function interceptFormData($oContactForm, $result)
    {
        global $oGEEntityManager;
        $mail_tags = array_flip($oContactForm->collect_mail_tags());

        foreach ($mail_tags as $item => $value) {
            if (isset($_POST[$item])) {
                $mail_tags[$item] = $_POST[$item];
            }
        }
        $success = false;
        $oSubmission = WPCF7_Submission::get_instance();
        $postedData = $oSubmission->get_posted_data();
        $r = 1;
        /*
                $log = new Logger('szkoda-input');
                $log->pushHandler(new StreamHandler(KoordynatorApiHandler::LOG_PATH, Logger::INFO));

                !$success ?
                    $log->error("Koordynator API PUT error, sessionID={$koordynatorApi->getSessionID()}")
                    : $log->info("Koordynator API PUT success, sessionID={$koordynatorApi->getSessionID()}");*/
    }

    public function checkForm()
    {
        if (!$this->formExists()) {
            $this->createForm();
        }
    }

    public function getFormID()
    {
        global $wpdb;
        $query = "SELECT `id` FROM {$wpdb->prefix}posts WHERE `post_type` LIKE 'wpcf7_contact_form' AND `post_status` = 'publish' AND `post_name` = '" . $this::SLUG . "';";
        $result = $wpdb->get_results($query, ARRAY_A);
        $formID = (int)array_shift($result)['id'];
        return $formID > 0 ? $formID : null;
    }

    private function formExists()
    {
        return null !== $this->getFormID();
    }

    private function createForm()
    {
        $contact_form = WPCF7_ContactForm::get_template();
        $contact_form->set_title($this::TITLE);

        $props = $contact_form->get_properties();
        $props['additional_settings'] = "demo_mode: on";
        $props['form'] = $this->getFormTemplate();
        $contact_form->set_properties($props);

        //here more form setup
        $contact_form->save();
    }

    private function get_localeMeta()
    {
        return "pl_PL";
    }

    private function get_additional_settingsMeta()
    {
        return NULL;
    }

    private function get_messagesMeta()
    {
        return [
            'mail_sent_ok' => 'Kalkulowanie, proszę czekać...',
            'mail_sent_ng' => 'There was an error trying to send your message. Please try again later.',
            'validation_error' => 'Nie wszystkie pola zostały wypełnione poprawnie.',
            'spam' => 'There was an error trying to send your message. Please try again later.',
            'accept_terms' => 'You must accept the terms and conditions before sending your message.',
            'invalid_required' => 'The field is required.',
            'invalid_too_long' => 'The field is too long.',
            'invalid_too_short' => 'The field is too short.',
        ];
    }

    private function getFormTemplate()
    {
        $statusManager = new insuranceCaseStatus();
        $statusList = $statusManager->getAllStatuses();
        $statusListStr = "\"" . implode("\" \"", $statusList) . "\"";
        return <<<EOT

[text cid default:get readonly class:hidden-input]
[text caid default:get readonly class:hidden-input]
[text calculation-created readonly class:hidden-input]
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-6">
        <label>Status sprawy
        [select* case-status default:get $statusListStr]</label></div>
        <div class="col-sm-6">
        <label>Opiekun sprawy
        [select* case-owner default:get]</label>
    </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
        <label>Imię i Nazwisko<span>*</span>
        [text client-full-name default:get]</label>
    </div>
    
        <div class="col-sm-6">
        <label>Numer telefonu<span>*</span>
        [text client-phone-number default:get]</label>
    </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
    <label>Adres e-mail<span>*</span>
        [text client-email default:get]</label></div>
    </div>
    
    <div class="row">
        <div class="col-sm-6">
        <label>Odszkodowanie z
        [select* insurance-type default:get "AC" "OC"]</label>
    </div>
        <div class="col-sm-6">
        <label>szkoda w Polsce?
        [select* collision-in-pl default:get "TAK" "NIE"]</label>
    </div>
    </div>
    
    
    <div class="row">
        <div class="col-sm-12">
        
    <label>Uwagi Rzeczoznawcy
        [textarea appraiser-case-description default:get]</label>    
    </div>
    </div>
      
    
    <div class="row">
        <div class="col-sm-12">
        
    <label>Uwagi Koordynatora
        [textarea manager-case-description default:get]</label>    
    </div>
    </div>
    
    
    
    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
        <label>Wartość odszkodowania
        [number* appraiser-proposed-value  min:0 default:get]</label>    
    </div>
        <div class="col-sm-12">
        <label>Wartość odszkodowania klienta
        [number client-proposed-value min:0  class:fake-input default:get]</label>    
    </div>
        <div class="col-sm-12">
        <label>Prowizja<span>*</span>
        [number margin-value min:0 readonly class:fake-input default:get]</label>
    </div>
    </div>
</div>





    



      
EOT;

    }

}